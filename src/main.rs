#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;
extern crate chrono;
extern crate strum;


use actix_web::{App, HttpServer};
use dotenv::dotenv;
use listenfd::ListenFd;
use std::env;
use actix_cors::Cors;
mod db;
mod documents;
mod templates;
mod templategroups;
mod templatestatuses;
mod variabledatatypes;
mod units;
mod variablecontenttypes;
mod variabledisplaytypes;
mod variableselectiontypes;
mod variables;
mod templatevariables;
mod documenttypes;
mod forms;
mod templateunits;
mod error_handler;
mod schema;


#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    dotenv().ok();
    db::init();
    let mut listenfd = ListenFd::from_env();
    let mut server = HttpServer::new(|| {
    // let cors = Cors::permissive().allow_any_origin();
    let cors = Cors::permissive();
    // let cors = Cors::default().allow_any_origin()
    //                 .allowed_methods(vec!["GET", "POST","PUT","DELETE"])
    //                 .allowed_headers(vec![http::header::AUTHORIZATION, http::header::ACCEPT])
    //                 .allowed_header(http::header::CONTENT_TYPE)
    //                 .max_age(3600);
        App::new()
            .wrap(cors)
            .app_data(actix_web::web::PayloadConfig::new(1000000 * 250))
            .configure(documents::init_routes)
            .configure(documenttypes::init_routes)
            .configure(templatestatuses::init_routes)
            .configure(variablecontenttypes::init_routes)
            .configure(variabledatatypes::init_routes)
            .configure(variableselectiontypes::init_routes)
            .configure(templateunits::init_routes)
            .configure(variabledisplaytypes::init_routes)
            .configure(templategroups::init_routes)
            .configure(templates::init_routes)
            .configure(units::init_routes)
            .configure(variables::init_routes)
            .configure(templatevariables::init_routes)
            .configure(forms::init_routes)
    });


    server = match listenfd.take_tcp_listener(0)? {
        Some(listener) => server.listen(listener)?,
        None => {
            let host = env::var("HOST").expect("Please set host in .env");
            let port = env::var("PORT").expect("Please set port in .env");
            server.bind(format!("{}:{}", host, port))?
        }
    };
    server.run().await
}
