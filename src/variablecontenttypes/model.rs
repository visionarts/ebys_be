use crate::db;
use crate::error_handler::CustomError;
use crate::schema::variablecontenttypes;
use diesel::prelude::*;
use serde::{Deserialize, Serialize};
use chrono::{NaiveDateTime};

#[derive(Serialize, Deserialize, AsChangeset, Insertable, Queryable)]
#[table_name = "variablecontenttypes"]
pub struct VariableContentType {
    pub content_type_name: Option<String>,
    pub created: Option<NaiveDateTime>,
    pub updated: Option<NaiveDateTime>
}

#[derive(Serialize, Deserialize, Queryable, Insertable)]
#[table_name = "variablecontenttypes"]
pub struct VariableContentTypes {
    pub id: i32,
    pub content_type_name: Option<String>,
    pub created: Option<NaiveDateTime>,
    pub updated: Option<NaiveDateTime>
}

impl VariableContentTypes {
    pub fn find_all() -> Result<Vec<Self>, CustomError> {
        let conn = db::connection()?;
        let template_variable_content_types = variablecontenttypes::table.load::<VariableContentTypes>(&conn)?;
        Ok(template_variable_content_types)
    }

    pub fn find(id: i32) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let template_variable_content_type = variablecontenttypes::table.filter(variablecontenttypes::id.eq(id)).first(&conn)?;
        Ok(template_variable_content_type)
    }

    pub fn create(template_variable_content_type: VariableContentType) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let template_variable_content_type = VariableContentType::from(template_variable_content_type);
        let template_variable_content_type = diesel::insert_into(variablecontenttypes::table)
            .values(template_variable_content_type)
            .get_result(&conn)?;
        Ok(template_variable_content_type)
    }

    pub fn update(id: i32, template_variable_content_type: VariableContentType) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let template_variable_content_type = diesel::update(variablecontenttypes::table)
            .filter(variablecontenttypes::id.eq(id))
            .set(template_variable_content_type)
            .get_result(&conn)?;
        Ok(template_variable_content_type)
    }

    pub fn delete(id: i32) -> Result<usize, CustomError> {
        let conn = db::connection()?;
        let res = diesel::delete(variablecontenttypes::table.filter(variablecontenttypes::id.eq(id))).execute(&conn)?;
        Ok(res)
    }

}

impl VariableContentType {
    fn from(template_variable_content_type: VariableContentType) -> VariableContentType {
        VariableContentType {
            content_type_name: template_variable_content_type.content_type_name,
            created: template_variable_content_type.created,
            updated: template_variable_content_type.updated
        }
    }
}
