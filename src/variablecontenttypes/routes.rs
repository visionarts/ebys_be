use crate::variablecontenttypes::{VariableContentType, VariableContentTypes};
use crate::error_handler::CustomError;
use actix_web::{delete, get, post, put, web, HttpResponse};
use serde_json::{json};

#[get("/sablondegiskeniicerikturleri")]
async fn find_all() -> Result<HttpResponse, CustomError> {
    let template_variable_content_types = VariableContentTypes::find_all()?;
    Ok(HttpResponse::Ok().json(template_variable_content_types))
}

#[get("/sablondegiskeniicerikturleri/{id}")]
async fn find(id: web::Path<i32>) -> Result<HttpResponse, CustomError> {
    let template_variable_content_type = VariableContentTypes::find(id.into_inner())?;
    Ok(HttpResponse::Ok().json(template_variable_content_type))
}

#[post("/sablondegiskeniicerikturleri")]
async fn create(template_variable_content_type: web::Json<VariableContentType>) -> Result<HttpResponse, CustomError> {
    let template_variable_content_type = VariableContentTypes::create(template_variable_content_type.into_inner())?;
    Ok(HttpResponse::Ok().json(template_variable_content_type))
}

#[put("/sablondegiskeniicerikturleri/{id}")]
async fn update(
    id: web::Path<i32>,
    template_variable_content_type: web::Json<VariableContentType>,
) -> Result<HttpResponse, CustomError> {
    let template_variable_content_type = VariableContentTypes::update(id.into_inner(), template_variable_content_type.into_inner())?;
    Ok(HttpResponse::Ok().json(template_variable_content_type))
}

#[delete("/sablondegiskeniicerikturleri/{id}")]
async fn delete(id: web::Path<i32>) -> Result<HttpResponse, CustomError> {
    let deleted_template_variable_content_type = VariableContentTypes::delete(id.into_inner())?;
    Ok(HttpResponse::Ok().json(json!({ "deleted template content type": deleted_template_variable_content_type })))
}

pub fn init_routes(comfig: &mut web::ServiceConfig) {
    comfig.service(find_all);
    comfig.service(find);
    comfig.service(create);
    comfig.service(update);
    comfig.service(delete);
}