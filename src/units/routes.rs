use crate::{units::{Unit, Units}};
use crate::error_handler::CustomError;
use actix_web::{delete, get, post, put, web, HttpResponse};
use serde_json::{json};

#[get("/birimler")]
async fn find_all() -> Result<HttpResponse, CustomError> {
    let units = Units::find_all(Some(50))?;
    Ok(HttpResponse::Ok().json(units))
}

#[get("/birimler/{id}")]
async fn find(id: web::Path<i32>) -> Result<HttpResponse, CustomError> {
    let unit = Units::find(id.into_inner())?;
    Ok(HttpResponse::Ok().json(unit))
}

#[post("/birimler")]
async fn create(unit: web::Json<Unit>) -> Result<HttpResponse, CustomError> {
    let unit = Units::create(unit.into_inner())?;
    Ok(HttpResponse::Ok().json(unit))
}

#[put("/birimler/{id}")]
async fn update(
    id: web::Path<i32>,
    unit: web::Json<Unit>,
) -> Result<HttpResponse, CustomError> {
    let unit = Units::update(id.into_inner(), unit.into_inner())?;
    Ok(HttpResponse::Ok().json(unit))
}

#[delete("/birimler/{id}")]
async fn delete(id: web::Path<i32>) -> Result<HttpResponse, CustomError> {
    let deleted_unit = Units::delete(id.into_inner())?;
    Ok(HttpResponse::Ok().json(json!({ "deleted unit": deleted_unit })))
}

pub fn init_routes(comfig: &mut web::ServiceConfig) {
    comfig.service(find_all);
    comfig.service(find);
    comfig.service(create);
    comfig.service(update);
    comfig.service(delete);
}