use crate::db;
use crate::error_handler::CustomError;
use crate::schema::units;
use crate::schema::units::dsl::*;
use diesel::prelude::*;
use serde::{Deserialize, Serialize};
use chrono::NaiveDateTime;

#[derive(Serialize, Deserialize, AsChangeset, Insertable, Queryable)]
#[table_name = "units"]
pub struct Unit {
    pub unit_name: Option<String>,
    pub visible_unit_name: Option<String>,
    pub main_administrative_id: Option<i32>,
    pub detsis_no: Option<i32>,
    pub unit_type_1: Option<i32>,
    pub unit_type_2: Option<i32>,
    pub hierarchy: Option<String>,
    pub parent_administrative_1: Option<i32>,
    pub parent_administrative_2: Option<i32>,
    pub tel: Option<String>,
    pub fax: Option<String>,
    pub address: Option<String>,
    pub email: Option<String>,
    pub web: Option<String>,
    pub hierarchy_in_documents: Option<String>,
    pub little_hierarchy: Option<String>,
    pub is_active: Option<bool>,
    pub address_information: Option<String>,
    pub created: Option<NaiveDateTime>,
    pub updated: Option<NaiveDateTime>
}

#[derive(Serialize, Deserialize, Queryable, Insertable,Clone)]
#[table_name = "units"]
pub struct Units {
    pub id: i32,
    pub unit_name: Option<String>,
    pub visible_unit_name: Option<String>,
    pub main_administrative_id: Option<i32>,
    pub detsis_no: Option<i32>,
    pub unit_type_1: Option<i32>,
    pub unit_type_2: Option<i32>,
    pub hierarchy: Option<String>,
    pub parent_administrative_1: Option<i32>,
    pub parent_administrative_2: Option<i32>,
    pub tel: Option<String>,
    pub fax: Option<String>,
    pub address: Option<String>,
    pub email: Option<String>,
    pub web: Option<String>,
    pub hierarchy_in_documents: Option<String>,
    pub little_hierarchy: Option<String>,
    pub is_active: Option<bool>,
    pub address_information: Option<String>,
    pub created: Option<NaiveDateTime>,
    pub updated: Option<NaiveDateTime>
}

impl Units {
    pub fn find_all(count: Option<i64>) -> Result<Vec<Self>, CustomError> {
        let conn = db::connection()?;
        let query = match count {
            None => units.load::<Units>(&conn).unwrap(),
            Some(count) => units.limit(count).load::<Units>(&conn).unwrap()
        };

        let _units = query;
        Ok(_units)
    }

    pub fn find(_id: i32) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let unit = units::table.filter(units::id.eq(_id)).first(&conn)?;
        Ok(unit)
    }

    pub fn create(unit: Unit) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let unit = Unit::from(unit);
        let unit = diesel::insert_into(units::table)
            .values(unit)
            .get_result(&conn)?;
        Ok(unit)
    }

    pub fn update(_id: i32, unit: Unit) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let unit = diesel::update(units::table)
            .filter(units::id.eq(_id))
            .set(unit)
            .get_result(&conn)?;
        Ok(unit)
    }

    pub fn delete(_id: i32) -> Result<usize, CustomError> {
        let conn = db::connection()?;
        let res = diesel::delete(units::table.filter(units::id.eq(_id))).execute(&conn)?;
        Ok(res)
    }
}

impl Unit {
    fn from(unit: Unit) -> Unit {
        Unit {
            unit_name: unit.unit_name,
            visible_unit_name: unit.visible_unit_name,
            main_administrative_id: unit.main_administrative_id,
            detsis_no: unit.detsis_no,
            unit_type_1: unit.unit_type_1,
            unit_type_2: unit.unit_type_2,
            hierarchy: unit.hierarchy,
            parent_administrative_1: unit.parent_administrative_1,
            parent_administrative_2: unit.parent_administrative_2,
            tel: unit.tel,
            fax: unit.fax,
            address: unit.address,
            email: unit.email,
            web: unit.web,
            hierarchy_in_documents: unit.hierarchy_in_documents,
            little_hierarchy: unit.little_hierarchy,
            is_active: unit.is_active,
            address_information: unit.address_information,
            created: unit.created,
            updated: unit.updated
        }
    }
}