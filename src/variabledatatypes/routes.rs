use crate::variabledatatypes;
use crate::variabledatatypes::{VariableDataType};
use crate::error_handler::CustomError;
use actix_web::{delete, get, post, put, web, HttpResponse};
use serde_json::{json};

#[get("/sablondegiskeniveritipleri")]
async fn find_all() -> Result<HttpResponse, CustomError> {
    let template_variable_data_types = variabledatatypes::VariableDataTypes::find_all()?;
    Ok(HttpResponse::Ok().json(template_variable_data_types))
}

#[get("/sablondegiskeniveritipleri/{id}")]
async fn find(id: web::Path<i32>) -> Result<HttpResponse, CustomError> {
    let template_variable_data_type = variabledatatypes::VariableDataTypes::find(id.into_inner())?;
    Ok(HttpResponse::Ok().json(template_variable_data_type))
}

#[post("/sablondegiskeniveritipleri")]
async fn create(template_variable_data_type: web::Json<VariableDataType>) -> Result<HttpResponse, CustomError> {
    let template_variable_data_type = variabledatatypes::VariableDataTypes::create(template_variable_data_type.into_inner())?;
    Ok(HttpResponse::Ok().json(template_variable_data_type))
}

#[put("/sablondegiskeniveritipleri/{id}")]
async fn update(
    id: web::Path<i32>,
    template_variable_data_type: web::Json<VariableDataType>,
) -> Result<HttpResponse, CustomError> {
    let template_variable_data_type = variabledatatypes::VariableDataTypes::update(id.into_inner(), template_variable_data_type.into_inner())?;
    Ok(HttpResponse::Ok().json(template_variable_data_type))
}

#[delete("/sablondegiskeniveritipleri/{id}")]
async fn delete(id: web::Path<i32>) -> Result<HttpResponse, CustomError> {
    let deleted_template_variable_data_type = variabledatatypes::VariableDataTypes::delete(id.into_inner())?;
    Ok(HttpResponse::Ok().json(json!({ "deleted template variable data type": deleted_template_variable_data_type })))
}

pub fn init_routes(comfig: &mut web::ServiceConfig) {
    comfig.service(find_all);
    comfig.service(find);
    comfig.service(create);
    comfig.service(update);
    comfig.service(delete);
}