use crate::db;
use crate::error_handler::CustomError;
use crate::schema::variabledatatypes;
use diesel::prelude::*;
use serde::{Deserialize, Serialize};
use chrono::{NaiveDateTime};

#[derive(Serialize, Deserialize, AsChangeset, Insertable, Queryable)]
#[table_name = "variabledatatypes"]
pub struct VariableDataType {
    pub data_type_name: Option<String>,
    pub created: Option<NaiveDateTime>,
    pub updated: Option<NaiveDateTime>
}

#[derive(Serialize, Deserialize, Queryable, Insertable)]
#[table_name = "variabledatatypes"]
pub struct VariableDataTypes {
    pub id: i32,
    pub data_type_name: Option<String>,
    pub created: Option<NaiveDateTime>,
    pub updated: Option<NaiveDateTime>
}

impl VariableDataTypes {
    pub fn find_all() -> Result<Vec<Self>, CustomError> {
        let conn = db::connection()?;
        let template_variable_data_types = variabledatatypes::table.load::<VariableDataTypes>(&conn)?;
        Ok(template_variable_data_types)
    }

    pub fn find(id: i32) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let template_variable_data_type = variabledatatypes::table.filter(variabledatatypes::id.eq(id)).first(&conn)?;
        Ok(template_variable_data_type)
    }

    pub fn create(template_variable_data_type: VariableDataType) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let template_variable_data_type = VariableDataType::from(template_variable_data_type);
        let template_variable_data_type = diesel::insert_into(variabledatatypes::table)
            .values(template_variable_data_type)
            .get_result(&conn)?;
        Ok(template_variable_data_type)
    }

    pub fn update(id: i32, template_variable_data_type: VariableDataType) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let template_variable_data_type = diesel::update(variabledatatypes::table)
            .filter(variabledatatypes::id.eq(id))
            .set(template_variable_data_type)
            .get_result(&conn)?;
        Ok(template_variable_data_type)
    }

    pub fn delete(id: i32) -> Result<usize, CustomError> {
        let conn = db::connection()?;
        let res = diesel::delete(variabledatatypes::table.filter(variabledatatypes::id.eq(id))).execute(&conn)?;
        Ok(res)
    }

}

impl VariableDataType {
    fn from(template_variable_data_type: VariableDataType) -> VariableDataType {
        VariableDataType {
            data_type_name: template_variable_data_type.data_type_name,
            created: template_variable_data_type.created,
            updated: template_variable_data_type.updated
        }
    }
}
