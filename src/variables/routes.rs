use crate::variables::{Variable, Variables};
use crate::error_handler::CustomError;
use actix_web::{delete, get, post, put, web, HttpResponse};
use serde_json::{json};

#[get("/degiskenler")]
async fn find_all() -> Result<HttpResponse, CustomError> {
    let variables = Variables::find_all()?;
    Ok(HttpResponse::Ok().json(variables))
}

#[get("/degiskenler/{id}")]
async fn find(id: web::Path<i32>) -> Result<HttpResponse, CustomError> {
    let variable = Variables::find(id.into_inner())?;
    Ok(HttpResponse::Ok().json(variable))
}

// #[post("/degiskenler")]
// async fn find_by_name(name: web::Json<Option<Vec<String>>>) -> Result<HttpResponse, CustomError> {
//     let variable_detail = Variables::find_by_name(name.into_inner())?;
//     Ok(HttpResponse::Ok().json(variable_detail))
// }

#[post("/degiskenler")]
async fn create(variable: web::Json<Variable>) -> Result<HttpResponse, CustomError> {
    let variable = Variables::create(variable.into_inner())?;
    Ok(HttpResponse::Ok().json(variable))
}

#[put("/degiskenler/{id}")]
async fn update(
    id: web::Path<i32>,
    variable: web::Json<Variable>,
) -> Result<HttpResponse, CustomError> {
    let variable = Variables::update(id.into_inner(), variable.into_inner())?;
    Ok(HttpResponse::Ok().json(variable))
}

#[delete("/degiskenler/{id}")]
async fn delete(id: web::Path<i32>) -> Result<HttpResponse, CustomError> {
    let deleted_variable = Variables::delete(id.into_inner())?;
    Ok(HttpResponse::Ok().json(json!({ "deleted template variable": deleted_variable })))
}

pub fn init_routes(comfig: &mut web::ServiceConfig) {
    comfig.service(find_all);
    comfig.service(find);
    comfig.service(create);
    comfig.service(update);
    comfig.service(delete);
}