use crate::db;
use crate::error_handler::CustomError;
use crate::schema::variables;
use chrono::NaiveDateTime;
use diesel::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Clone,Debug,Serialize, Deserialize, AsChangeset, Insertable, Queryable)]
#[table_name = "variables"]
pub struct Variable {
    pub order_num: Option<i32>,
    pub data_type_id: Option<i32>,
    pub content_type_id: Option<i32>,
    pub display_type_id:Option< i32>,
    pub selection_type_id: Option<i32>,
    pub variable_name: Option<String>,
    pub unit_id: Option<i32>,
    pub explanation: Option<String>,
    pub is_system_variable: Option<bool>,
    pub is_mandatory: Option<bool>,
    pub created: Option<NaiveDateTime>,
    pub updated: Option<NaiveDateTime>
}

#[derive(Clone,Debug,Serialize, Deserialize, Queryable, Insertable)]
#[table_name = "variables"]
pub struct Variables {
    pub id: i32,
    pub order_num: Option<i32>,
    pub data_type_id: Option<i32>,
    pub content_type_id: Option<i32>,
    pub display_type_id:Option< i32>,
    pub selection_type_id: Option<i32>,
    pub variable_name: Option<String>,
    pub unit_id: Option<i32>,
    pub explanation: Option<String>,
    pub is_system_variable: Option<bool>,
    pub is_mandatory: Option<bool>,
    pub created: Option<NaiveDateTime>,
    pub updated: Option<NaiveDateTime>
}

impl Variables {
    pub fn find_all() -> Result<Vec<Self>, CustomError> {
        let conn = db::connection()?;
        let variables = variables::table.load::<Variables>(&conn)?;
        Ok(variables)
    }

    pub fn find(id: i32) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let variable = variables::table
            .filter(variables::id.eq(id))
            .first(&conn)?;
        Ok(variable)
    }

    pub fn find_by_name(_name: Option<Vec<String>>) -> Result<Vec<(i32,Option<String>,Option<bool>)>, CustomError> {
        let conn = db::connection()?;
        let variable_details = variables::table
            .filter(variables::variable_name.eq_any(_name.unwrap()))
            .select((variables::id,variables::variable_name,variables::is_system_variable))
            .get_results::<(i32,Option<String>,Option<bool>)>(&conn)?;
        Ok(variable_details)
    }

    pub fn create(variable: Variable) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let variable = Variable::from(variable);
        let variable = diesel::insert_into(variables::table)
            .values(variable)
            .get_result(&conn)?;
        Ok(variable)
    }

    pub fn update(id: i32, variable: Variable) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let variable = diesel::update(variables::table)
            .filter(variables::id.eq(id))
            .set(variable)
            .get_result(&conn)?;
        Ok(variable)
    }

    pub fn delete(id: i32) -> Result<usize, CustomError> {
        let conn = db::connection()?;
        let res = diesel::delete(variables::table.filter(variables::id.eq(id)))
            .execute(&conn)?;
        Ok(res)
    }
}

impl Variable {
    fn from(variable: Variable) -> Variable {
        Variable {
            order_num: variable.order_num,
            data_type_id: variable.data_type_id,
            content_type_id: variable.content_type_id,
            display_type_id: variable.display_type_id,
            selection_type_id: variable.selection_type_id,
            variable_name: variable.variable_name,
            unit_id: variable.unit_id,
            explanation: variable.explanation,
            is_system_variable: variable.is_system_variable,
            is_mandatory: variable.is_mandatory,
            created: variable.created,
            updated: variable.updated,
        }
    }
}
