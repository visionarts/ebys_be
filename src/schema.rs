table! {
    documents (id) {
        id -> Int4,
        document_name -> Varchar,
        document_body -> Varchar,
        document_subject -> Varchar,
        archieve_code -> Int4,
        archieve_code_special -> Int4,
        confidentiality -> Int4,
        urgency -> Int4,
        created -> Nullable<Timestamptz>,
        updated -> Nullable<Timestamptz>,
    }
}

table! {
    documenttypes (id) {
        id -> Int4,
        document_type -> Nullable<Varchar>,
        is_physical_document -> Nullable<Bool>,
        created -> Nullable<Timestamptz>,
        updated -> Nullable<Timestamptz>,
    }
}

table! {
    forms (id) {
        id -> Int4,
        form_name -> Nullable<Varchar>,
        created -> Nullable<Timestamptz>,
        updated -> Nullable<Timestamptz>,
    }
}

table! {
    templategroups (id) {
        id -> Int4,
        group_name -> Nullable<Varchar>,
        created -> Nullable<Timestamptz>,
        updated -> Nullable<Timestamptz>,
    }
}

table! {
    templates (template_id) {
        template_id -> Int4,
        template_group_id -> Nullable<Int4>,
        document_type_id -> Nullable<Int4>,
        template_status_id -> Nullable<Int4>,
        template_name -> Nullable<Varchar>,
        template_file_name -> Nullable<Varchar>,
        template_vars -> Nullable<Array<Text>>,
        created -> Nullable<Timestamptz>,
        updated -> Nullable<Timestamptz>,
    }
}

table! {
    templatestatuses (id) {
        id -> Int4,
        status_name -> Nullable<Varchar>,
        created -> Nullable<Timestamptz>,
        updated -> Nullable<Timestamptz>,
    }
}

table! {
    templateunits (id) {
        id -> Int4,
        unit_id -> Int4,
        template_id -> Int4,
        created -> Nullable<Timestamptz>,
        updated -> Nullable<Timestamptz>,
    }
}

table! {
    templatevariables (id) {
        id -> Int4,
        template_id -> Int4,
        variable_id -> Int4,
        form_id -> Int4,
        created -> Nullable<Timestamptz>,
        updated -> Nullable<Timestamptz>,
    }
}

table! {
    units (id) {
        id -> Int4,
        unit_name -> Nullable<Varchar>,
        visible_unit_name -> Nullable<Varchar>,
        main_administrative_id -> Nullable<Int4>,
        detsis_no -> Nullable<Int4>,
        unit_type_1 -> Nullable<Int4>,
        unit_type_2 -> Nullable<Int4>,
        hierarchy -> Nullable<Varchar>,
        parent_administrative_1 -> Nullable<Int4>,
        parent_administrative_2 -> Nullable<Int4>,
        tel -> Nullable<Varchar>,
        fax -> Nullable<Varchar>,
        address -> Nullable<Varchar>,
        email -> Nullable<Varchar>,
        web -> Nullable<Varchar>,
        hierarchy_in_documents -> Nullable<Varchar>,
        little_hierarchy -> Nullable<Varchar>,
        is_active -> Nullable<Bool>,
        address_information -> Nullable<Varchar>,
        created -> Nullable<Timestamptz>,
        updated -> Nullable<Timestamptz>,
    }
}

table! {
    variablecontenttypes (id) {
        id -> Int4,
        content_type_name -> Nullable<Varchar>,
        created -> Nullable<Timestamptz>,
        updated -> Nullable<Timestamptz>,
    }
}

table! {
    variabledatatypes (id) {
        id -> Int4,
        data_type_name -> Nullable<Varchar>,
        created -> Nullable<Timestamptz>,
        updated -> Nullable<Timestamptz>,
    }
}

table! {
    variabledisplaytypes (id) {
        id -> Int4,
        display_type_name -> Nullable<Varchar>,
        created -> Nullable<Timestamptz>,
        updated -> Nullable<Timestamptz>,
    }
}

table! {
    variables (id) {
        id -> Int4,
        order_num -> Nullable<Int4>,
        data_type_id -> Nullable<Int4>,
        content_type_id -> Nullable<Int4>,
        display_type_id -> Nullable<Int4>,
        selection_type_id -> Nullable<Int4>,
        variable_name -> Nullable<Varchar>,
        unit_id -> Nullable<Int4>,
        explanation -> Nullable<Varchar>,
        is_system_variable -> Nullable<Bool>,
        is_mandatory -> Nullable<Bool>,
        created -> Nullable<Timestamptz>,
        updated -> Nullable<Timestamptz>,
    }
}

table! {
    variableselectiontypes (id) {
        id -> Int4,
        selection_type_name -> Nullable<Varchar>,
        created -> Nullable<Timestamptz>,
        updated -> Nullable<Timestamptz>,
    }
}

joinable!(templates -> documenttypes (document_type_id));
joinable!(templates -> templategroups (template_group_id));
joinable!(templates -> templatestatuses (template_status_id));
joinable!(templateunits -> templates (template_id));
joinable!(templateunits -> units (unit_id));
joinable!(templatevariables -> forms (form_id));
joinable!(templatevariables -> templates (template_id));
joinable!(templatevariables -> variables (variable_id));
joinable!(variables -> units (unit_id));
joinable!(variables -> variablecontenttypes (content_type_id));
joinable!(variables -> variabledatatypes (data_type_id));
joinable!(variables -> variabledisplaytypes (display_type_id));
joinable!(variables -> variableselectiontypes (selection_type_id));

allow_tables_to_appear_in_same_query!(
    documents,
    documenttypes,
    forms,
    templategroups,
    templates,
    templatestatuses,
    templateunits,
    templatevariables,
    units,
    variablecontenttypes,
    variabledatatypes,
    variabledisplaytypes,
    variables,
    variableselectiontypes,
);
