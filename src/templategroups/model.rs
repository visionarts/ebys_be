use crate::db;
use crate::error_handler::CustomError;
use crate::schema::templategroups;
use diesel::prelude::*;
use serde::{Deserialize, Serialize};
use chrono::{NaiveDateTime};

#[derive(Serialize, Deserialize, AsChangeset, Insertable, Queryable,Associations,Clone)]
#[table_name = "templategroups"]
pub struct TemplateGroup {
    pub group_name: Option<String>,
    pub created: Option<NaiveDateTime>, 
    pub updated: Option<NaiveDateTime> 
}

#[derive(Serialize, Deserialize, Queryable, Insertable,Associations,Clone)]
#[table_name = "templategroups"]
pub struct TemplateGroups {
    pub id: i32,
    pub group_name: Option<String>,
    pub created: Option<NaiveDateTime>, 
    pub updated: Option<NaiveDateTime> 
}

impl TemplateGroups {
    pub fn find_all() -> Result<Vec<Self>, CustomError> {
        let conn = db::connection()?;
        let template_groupes = templategroups::table.load::<TemplateGroups>(&conn)?;
        Ok(template_groupes)
    }

    pub fn find(id: i32) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let template_group = templategroups::table.filter(templategroups::id.eq(id)).first(&conn)?;
        Ok(template_group)
    }

    pub fn create(template_group: TemplateGroup) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let template_group = TemplateGroup::from(template_group);
        let template_group = diesel::insert_into(templategroups::table)
            .values(template_group)
            .get_result(&conn)?;
        Ok(template_group)
    }

    pub fn update(id: i32, template_group: TemplateGroup) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let template_group = diesel::update(templategroups::table)
            .filter(templategroups::id.eq(id))
            .set(template_group)
            .get_result(&conn)?;
        Ok(template_group)
    }

    pub fn delete(id: i32) -> Result<usize, CustomError> {
        let conn = db::connection()?;
        let res = diesel::delete(templategroups::table.filter(templategroups::id.eq(id))).execute(&conn)?;
        Ok(res)
    }

}

impl TemplateGroup {
    fn from(template_group: TemplateGroup) -> TemplateGroup {
        TemplateGroup {
            group_name: template_group.group_name,
            created: template_group.created,
            updated: template_group.updated
        }
    }
}
