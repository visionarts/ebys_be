use crate::templategroups::{TemplateGroup, TemplateGroups};
use crate::error_handler::CustomError;
use actix_web::{delete, get, post, put, web, HttpResponse};
use serde_json::{json};

#[get("/sablongruplari")]
async fn find_all() -> Result<HttpResponse, CustomError> {
    let template_groupes = TemplateGroups::find_all()?;
    Ok(HttpResponse::Ok().json(template_groupes))
}

#[get("/sablongruplari/{id}")]
async fn find(id: web::Path<i32>) -> Result<HttpResponse, CustomError> {
    let template_group = TemplateGroups::find(id.into_inner())?;
    Ok(HttpResponse::Ok().json(template_group))
}

#[post("/sablongruplari")]
async fn create(template_group: web::Json<TemplateGroup>) -> Result<HttpResponse, CustomError> {
    let template_group = TemplateGroups::create(template_group.into_inner())?;
    Ok(HttpResponse::Ok().json(template_group))
}

#[put("/sablongruplari/{id}")]
async fn update(
    id: web::Path<i32>,
    template_group: web::Json<TemplateGroup>,
) -> Result<HttpResponse, CustomError> {
    let template_group = TemplateGroups::update(id.into_inner(), template_group.into_inner())?;
    Ok(HttpResponse::Ok().json(template_group))
}

#[delete("/sablongruplari/{id}")]
async fn delete(id: web::Path<i32>) -> Result<HttpResponse, CustomError> {
    let deleted_template_group = TemplateGroups::delete(id.into_inner())?;
    Ok(HttpResponse::Ok().json(json!({ "deleted template group": deleted_template_group })))
}

pub fn init_routes(comfig: &mut web::ServiceConfig) {
    comfig.service(find_all);
    comfig.service(find);
    comfig.service(create);
    comfig.service(update);
    comfig.service(delete);
}