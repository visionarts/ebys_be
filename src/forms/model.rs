use crate::schema::forms::dsl::*;
use crate::db;
use crate::error_handler::CustomError;
use crate::schema::forms;
use diesel::prelude::*;
use serde::{Deserialize, Serialize};
use chrono::{NaiveDateTime};




#[derive(Serialize, Deserialize, AsChangeset, Insertable, Queryable,Clone)]
#[table_name = "forms"]
pub struct Form {
    pub form_name: Option<String>,
    pub created: Option<NaiveDateTime>, 
    pub updated: Option<NaiveDateTime> 
}

#[derive(Serialize, Deserialize, Queryable, Insertable,Clone)]
#[table_name = "forms"]
pub struct Forms {
    pub id: i32,
    pub form_name: Option<String>,
    pub created: Option<NaiveDateTime>, 
    pub updated: Option<NaiveDateTime> 

}

impl Forms {
    pub fn find_all() -> Result<Vec<Self>, CustomError> {
        let conn = db::connection()?;
        let _forms = forms::table.load::<Forms>(&conn)?;
        Ok(_forms)
    }

    pub fn find(_id: i32) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let form = forms::table.filter(forms::id.eq(_id)).first(&conn)?;
        Ok(form)
    }

    pub fn find_by_id(_form_ids: Option<Vec<i32>>) -> Result<Vec<(i32,Option<String>)>, CustomError> {
        let conn = db::connection()?;
        let _forms = forms::table
            .filter(forms::id.eq_any(_form_ids.unwrap()))
            .select((forms::id,forms::form_name))
            .get_results::<(i32,Option<String>)>(&conn)?;
        Ok(_forms)
    }

    pub fn create(form: Form) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let form = Form::from(form);
        let form = diesel::insert_into(forms::table)
            .values(form)
            .get_result(&conn)?;
        Ok(form)
    }

    pub fn create_bulk(_forms: Vec<Form>) -> Result<Vec<i32>, CustomError> {
        let conn = db::connection()?;

        let inserted_forms: Vec<i32> = diesel::insert_into(forms)
            .values(&_forms)
            .returning(id)
            .get_results(&conn)?;

        Ok(inserted_forms)
    }

    pub fn update(_id: i32, form: Form) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let form = diesel::update(forms::table)
            .filter(forms::id.eq(_id))
            .set(form)
            .get_result(&conn)?;
        Ok(form)
    }

    pub fn update_bulk(_forms: Vec<Forms>) -> Result<Vec<Forms>, CustomError> {
        let forms_toupd: Vec<Forms> = _forms.clone();
        let mut res :Vec<Forms> = Vec::new();
        for n in 0..forms_toupd.len(){
            let form_node: Forms = forms_toupd.get(n).unwrap().clone();
            let form_toupd =  Form {
                form_name: Some(form_node.form_name.unwrap()),
                created: None,
                updated: None
            };

            res.push(Self::update(form_node.id, form_toupd).unwrap());
        }

        Ok(res)
    }

    pub fn delete(_id: i32) -> Result<usize, CustomError> {
        let conn = db::connection()?;
        let res = diesel::delete(forms::table.filter(forms::id.eq(_id))).execute(&conn)?;
        Ok(res)
    }

    pub fn delete_bulk(_ids: Vec<i32>) -> Result<usize, CustomError> {
        let conn = db::connection()?;
        let res = diesel::delete(forms::table.filter(forms::id.eq_any(_ids))).execute(&conn)?;
        Ok(res)
    }

}

impl Form {
    fn from(form: Form) -> Form {
        Form {
            form_name: form.form_name,
            created: form.created,
            updated: form.updated
        }
    }
}