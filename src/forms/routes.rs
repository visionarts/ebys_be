use crate::forms::{Form, Forms};
use crate::error_handler::CustomError;
use actix_web::{delete, get, post, put, web, HttpResponse};
use serde_json::{json};

#[get("/formlar")]
async fn find_all() -> Result<HttpResponse, CustomError> {
    let forms = Forms::find_all()?;
    Ok(HttpResponse::Ok().json(forms))
}

#[get("/formlar/{id}")]
async fn find(id: web::Path<i32>) -> Result<HttpResponse, CustomError> {
    let form = Forms::find(id.into_inner())?;
    Ok(HttpResponse::Ok().json(form))
}

#[post("/formlar")]
async fn create(form: web::Json<Form>) -> Result<HttpResponse, CustomError> {
    let form = Forms::create(form.into_inner())?;
    Ok(HttpResponse::Ok().json(form))
}

#[put("/formlar/{id}")]
async fn update(
    id: web::Path<i32>,
    form: web::Json<Form>,
) -> Result<HttpResponse, CustomError> {
    let form = Forms::update(id.into_inner(), form.into_inner())?;
    Ok(HttpResponse::Ok().json(form))
}

#[delete("/formlar/{id}")]
async fn delete(id: web::Path<i32>) -> Result<HttpResponse, CustomError> {
    let deleted_form = Forms::delete(id.into_inner())?;
    Ok(HttpResponse::Ok().json(json!({ "deleted form": deleted_form })))
}

pub fn init_routes(config: &mut web::ServiceConfig) {
    config.service(find_all);
    config.service(find);
    config.service(create);
    config.service(update);
    config.service(delete);
}