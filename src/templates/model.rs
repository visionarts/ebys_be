use std::collections::HashSet;
use crate::templateunits::TemplateUnits;
use crate::templateunits::TemplateUnit;
use crate::templates::Table;
use crate::units::Units;
use crate::templatevariables::TemplateVariables;
use crate::forms::Forms;
use crate::templategroups::TemplateGroup;
use crate::documenttypes::DocumentTypes;
use crate::templategroups::TemplateGroups;
use crate::templatestatuses::TemplateStatuses;
use crate::schema::templatestatuses::dsl::templatestatuses as ts;
use crate::schema::templategroups::dsl::templategroups as tg;
use crate::schema::documenttypes::dsl::documenttypes as dt;
use crate::variables;
use crate::{documenttypes,templategroups, templateunits::{self},templatevariables::{self, TemplateVariable}, units};
use crate::forms::Form;
use crate::error_handler::CustomError;
use crate::schema::templates;
use crate::schema::templates::dsl::*;
use crate::{db,forms};
use actix_multipart::Multipart;
use actix_web::{web, Error};
use chrono::NaiveDateTime;
use diesel::prelude::*;
use futures::{StreamExt, TryStreamExt};
use serde::{Deserialize, Serialize};
use std::env;
use std::{io::Write, process::Command};

use super::*;

#[derive(Debug,Serialize, Deserialize, Insertable, Queryable, AsChangeset,Associations,Clone)]
#[table_name = "templates"]
#[belongs_to(TemplateGroup)]
pub struct Template {
    pub template_group_id:  Option<i32>,
    pub document_type_id:  Option<i32>,
    pub template_status_id:  Option<i32>,
    pub template_name: Option<String>,
    pub template_file_name: Option<String>,
    pub template_vars: Option<Vec<String>>,
    pub created: Option<NaiveDateTime>,
    pub updated: Option<NaiveDateTime>
}

#[derive(Debug,Serialize, Deserialize, Queryable, Insertable,AsChangeset,Associations,Clone)]
#[table_name = "templates"]
#[belongs_to(TemplateGroup)]
pub struct Templates {
    pub template_id: i32,
    pub template_group_id:  Option<i32>,
    pub document_type_id:  Option<i32>,
    pub template_status_id:  Option<i32>,
    pub template_name: Option<String>,
    pub template_file_name: Option<String>,
    pub template_vars: Option<Vec<String>>,
    pub created: Option<NaiveDateTime>,
    pub updated: Option<NaiveDateTime>
}

impl Templates {
    pub fn find_all() -> Result<Vec<Table>, CustomError> {
        let conn = db::connection()?;

        let _templates: Vec<(Templates,TemplateGroups,TemplateStatuses,DocumentTypes)> = templates.inner_join(tg).inner_join(ts).inner_join(dt)
        .load(&conn)?;

        let mut _table : Vec<Table> = Vec::new();

        for n in 0.._templates.len() {
            let templ_grp_st_dt = _templates.get(n).unwrap();
            let template:Templates = templ_grp_st_dt.0.clone();
            let _template_group: TemplateGroups = templ_grp_st_dt.1.clone();
            let _template_status: TemplateStatuses = templ_grp_st_dt.2.clone();
            let _document_type: DocumentTypes = templ_grp_st_dt.3.clone();

            let _template_file_name = template.template_file_name.unwrap();
            let _template_id = template.template_id;
            let _template_name = template.template_name.unwrap();

            let _units: Vec<Units> = TemplateUnits::find_by_templateid(_template_id).unwrap();
            let _template_variables: Vec<TemplateVariable1> = TemplateVariables::get_variables_by_templateid(_template_id).unwrap();
            let template_forms: Vec<Forms> = TemplateVariables::get_forms_by_templateid(_template_id).unwrap();

            let mut _forms_on_templates: Vec<FormOnTemplates> = template_forms.into_iter().map(|i| FormOnTemplates {
            form_id: i.id,
            form_name: i.form_name.unwrap(),
            template_variables: TemplateVariables::get_variables_by_formid(i.id).unwrap()
            }).collect();

            let _table_node = Table {
                template_id: _template_id,
                template_name: _template_name,
                template_group: _template_group,
                template_status: _template_status,
                document_type: _document_type,
                units: _units,
                template_variables:_template_variables,
                forms: _forms_on_templates,
                file_name: _template_file_name
            };

            _table.push(_table_node);
        }

        Ok(_table)
    }

    pub fn get_sablonlar_as_grouped() -> Result<Vec<GroupedTemplates>, CustomError> {
        let _conn = db::connection()?;
        let _template_groups: Vec<TemplateGroups> = TemplateGroups::find_all().unwrap();
        let _templates: Vec<Table> = Self::find_all().unwrap();
        let mut _grouped_templates: Vec<GroupedTemplates> = Vec::new();
        for n in 0.._template_groups.len() {
            let current_template_group = &(_template_groups.get(n));
            let group_templates: Vec<Table> = _templates.iter().filter(|&x| x.template_group.id == (*current_template_group).unwrap().id.clone()).cloned().collect();
            
            let group = GroupedTemplates {
                template_group_id: (*current_template_group).unwrap().id.clone(),
                template_group_name: (*current_template_group).unwrap().group_name.as_ref().unwrap().to_string(),
                templates: group_templates
            };
            _grouped_templates.push(group);
        }

        let _grouped_templates: Vec<GroupedTemplates> = _grouped_templates
        .iter()
        .filter(|&x| x.templates.len() > 0)
        .cloned()
        .collect();
        
        Ok(_grouped_templates)
    }

    pub fn find_group_type_unit() -> Result<GroupUnitType, CustomError> {
        let _template_groups = templategroups::TemplateGroups::find_all();
        let _units = units::Units::find_all(Some(50));
        let _document_types = documenttypes::DocumentTypes::find_all();

        let _group_type_unit = GroupUnitType {
            template_groups: _template_groups.unwrap(),
            units: _units.unwrap(),
            document_types: _document_types.unwrap()
        };

        Ok(_group_type_unit)
    }

    pub fn find(_id: i32) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let _template = templates::table
            .filter(templates::template_id.eq(_id))
            .first(&conn)?;
        Ok(_template)
    }

    pub fn save_template(template: Templates) -> Result<TemplatesDetailed, CustomError> {
        let conn = db::connection()?;
        let _template_group_id = &template.template_group_id;
        let _document_type_id = &template.document_type_id;
        let _template_status_id = &template.template_status_id;
        let _template_name = &template.template_name;
        let _template_file_name = &template.template_file_name;

        let new_file = (
            template_group_id.eq(_template_group_id),
            document_type_id.eq(_document_type_id),
            template_status_id.eq(_template_status_id),
            template_name.eq(_template_name),
            template_file_name.eq(_template_file_name)
        );

        let _template_id = diesel::insert_into(templates)
            .values(&new_file)
            .returning(template_id)
            .get_result(&conn)?;

        let _test_res =
            variables::Variables::find_by_name(template.template_vars).unwrap();
        let mut formatted_variables: Vec<TemplateVariable1> = Vec::new();

        for n in 0.._test_res.len() {
            let template_var = _test_res.get(n).unwrap().clone();
            let templ_var_id = template_var.0;
            let templ_var_name = template_var.1;
            let templ_var_group = template_var.2;

            let _group = match (templ_var_group).unwrap() {
                true => Some("System".to_string()),
                false => Some("Ebys".to_string()),
            };

            let templ_var = TemplateVariable1 {
                id: Some(templ_var_id),
                variable_name: templ_var_name,
                group: _group,
                valid: Some(true),
            };
            formatted_variables.push(templ_var);
        }
        let _template = TemplatesDetailed {
            id: _template_id,
            template_group_id: template.template_group_id,
            document_type_id: template.document_type_id,
            template_status_id: template.template_status_id,
            template_name: template.template_name,
            template_file_name: template.template_file_name,
            created: None,
            updated: None
        };

        Ok(_template)
    }

    pub async fn upload_template(template_variables_to_save: TemplateVariablesToSave) -> Result<i32, Error> {
        let forms_from_fe : Vec<TemplateForm> = template_variables_to_save.forms.clone();
        
        let formnames_tosave: Vec<Form> = forms_from_fe.into_iter().map(|i| Form {
            form_name: Some(i.form_name),
            created: None,
            updated: None
        } ).collect();

        let formids_saved = forms::Forms::create_bulk(formnames_tosave);
        let forms_saved = forms::Forms::find_by_id(Some(formids_saved.unwrap())).unwrap();

        let template = Template {
            template_name: Some(template_variables_to_save.template_name),
            template_file_name: Some(template_variables_to_save.template_file_name),
            template_group_id: Some(template_variables_to_save.template_group_id),
            template_status_id: Some(template_variables_to_save.template_status_id),
            document_type_id: Some(template_variables_to_save.document_type_id),
            template_vars: Some(Vec::new()),
            created: None,
            updated: None
        };

        let template_id_inserted = Self::create(template).unwrap();
        let mut _tmplt2tmplt_vars: Vec<TemplateVariable> = Vec::new();
        for n in 0..template_variables_to_save.forms.len()
        {
            let formvariables_tosave = template_variables_to_save.forms.get(n).clone();
            let find_form_record: Vec<(i32,Option<String>)> = forms_saved.clone()
            .iter()
            .filter(|&x| x.clone().1.unwrap() == formvariables_tosave.unwrap().form_name)
            .cloned()
            .collect();

            let mut _tmplt2tmplt_vars_to_save: Vec<TemplateVariable> = formvariables_tosave.unwrap().variables.clone().into_iter().map(|i| TemplateVariable {
            template_id: template_id_inserted,
            variable_id: i,
            form_id: find_form_record.get(0).unwrap().0,
            created: None,
            updated: None
            }).collect();

            _tmplt2tmplt_vars.append(&mut _tmplt2tmplt_vars_to_save);
        };

        let _form_variables: Vec<i32> = _tmplt2tmplt_vars.clone().into_iter().map(|i| i.variable_id).collect();
        
        let _form_variables_hashset: HashSet<_> = _form_variables.iter().collect();
        let _unassigned_variable_ids: Vec<i32> = template_variables_to_save.variables.clone().into_iter().filter(|item| !_form_variables_hashset.contains(item)).collect();
        
        let mut _unassigned_variables: Vec<TemplateVariable> = _unassigned_variable_ids.into_iter().map(|i| TemplateVariable {
            template_id: template_id_inserted,
            variable_id: i,
            form_id: -1,
            created: None,
            updated: None
            }).collect();
            
         _tmplt2tmplt_vars.append(&mut _unassigned_variables);

        let unit_ids : Vec<i32> = template_variables_to_save.units.clone();
        let template_units: Vec<TemplateUnit> = unit_ids.into_iter().map(|i| TemplateUnit {
            template_id: template_id_inserted,
            unit_id: i,
            created: None,
            updated: None
            }).collect();

        let _saved_templateunit_ids: Vec<i32> = templateunits::TemplateUnits::create_bulk(template_units).unwrap();
        let _saved_template_ids: Vec<i32> = templatevariables::TemplateVariables::create_bulk(_tmplt2tmplt_vars).unwrap();

        Ok(template_id_inserted)
    }

    pub fn create(template: Template) -> Result<i32, CustomError> {
        let conn = db::connection()?;
        let _template = Template::from(template);
        let result = diesel::insert_into(templates::table)
            .values(_template)
            .returning(template_id)
            .get_result(&conn)?;
        Ok(result)
    }

    pub fn update_template(id:i32, template_variables_to_save: TemplateVariablesToSave) -> Result<i32, CustomError> {
        // 1 delete template units
        TemplateUnits::delete_by_template_id(id).unwrap();
        // 2 delete templatevariables
        TemplateVariables::delete_by_template_id(id).unwrap();
        // 3 delete forms
        let form_ids_todelete: Vec<i32> = template_variables_to_save.clone().forms.into_iter().map(|i| i.form_id.unwrap()).collect();
        Forms::delete_bulk(form_ids_todelete).unwrap();

        let forms_from_fe : Vec<TemplateForm> = template_variables_to_save.forms.clone();
        
        let formnames_tosave: Vec<Form> = forms_from_fe.into_iter().map(|i| Form {
            form_name: Some(i.form_name),
            created: None,
            updated: None
        } ).collect();

        let formids_saved = forms::Forms::create_bulk(formnames_tosave);
        let forms_saved = forms::Forms::find_by_id(Some(formids_saved.unwrap())).unwrap();

        let template = Template {
            template_name: Some(template_variables_to_save.template_name),
            template_file_name: Some(template_variables_to_save.template_file_name),
            template_group_id: Some(template_variables_to_save.template_group_id),
            template_status_id: Some(template_variables_to_save.template_status_id),
            document_type_id: Some(template_variables_to_save.document_type_id),
            template_vars: Some(Vec::new()),
            created: None,
            updated: None
        };

        Self::update(id, template).unwrap(); //Template Update
        let mut _tmplt2tmplt_vars: Vec<TemplateVariable> = Vec::new();
        for n in 0..template_variables_to_save.forms.len()
        {
            let formvariables_tosave = template_variables_to_save.forms.get(n).clone();
            let find_form_record: Vec<(i32,Option<String>)> = forms_saved.clone()
            .iter()
            .filter(|&x| x.clone().1.unwrap() == formvariables_tosave.unwrap().form_name)
            .cloned()
            .collect();

            let mut _tmplt2tmplt_vars_to_save: Vec<TemplateVariable> = formvariables_tosave.unwrap().variables.clone().into_iter().map(|i| TemplateVariable {
            template_id: id,
            variable_id: i,
            form_id: find_form_record.get(0).unwrap().0,
            created: None,
            updated: None
            }).collect();

            

            _tmplt2tmplt_vars.append(&mut _tmplt2tmplt_vars_to_save);
        };

        let unit_ids : Vec<i32> = template_variables_to_save.units.clone();
        let template_units: Vec<TemplateUnit> = unit_ids.into_iter().map(|i| TemplateUnit {
            template_id: id,
            unit_id: i,
            created: None,
            updated: None
            }).collect();

        let _saved_templateunit_ids: Vec<i32> = templateunits::TemplateUnits::create_bulk(template_units).unwrap();
        let _saved_template_ids: Vec<i32> = templatevariables::TemplateVariables::create_bulk(_tmplt2tmplt_vars).unwrap();

        Ok(id)
    }

    pub fn update(_id: i32, template: Template) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let template = diesel::update(templates::table)
            .filter(templates::template_id.eq(_id))
            .set(template)
            .get_result(&conn)?;
        Ok(template)
    }

    pub fn delete(_id: i32) -> Result<usize, CustomError> {
        let conn = db::connection()?;
        // 1 delete template units
        TemplateUnits::delete_by_template_id(_id).unwrap();
        // 2 delete templatevariables
        let forms: Vec<Forms> = TemplateVariables::get_forms_by_templateid(_id).unwrap();
        TemplateVariables::delete_by_template_id(_id).unwrap();
        // 3 delete forms
        let form_ids_todelete: Vec<i32> = forms.into_iter().map(|i| i.id).collect();
        Forms::delete_bulk(form_ids_todelete).unwrap();
        // delete template
        let res4 = diesel::delete(templates::table.filter(templates::template_id.eq(_id))).execute(&conn)?;

        Ok(res4)
    }

    pub async fn temporary_upload(
        mut payload: Multipart,
    ) -> Result<TemplateTemporaryUploadedVariables, Error> {
        // iterate over multipart stream
        let mut res = TemplateTemporaryUploadedVariables {
            file_name: None,
            template_vars: Vec::new(),
        };

        while let Ok(Some(mut field)) = payload.try_next().await {
            let content_type = field.content_disposition().unwrap();
            let filename = content_type.get_filename().unwrap();
            let dll = env::var("TEMPLATE").expect("Please set TEMPLATE in .env");
            let filepath = format!("{}{}", dll, sanitize_filename::sanitize(&filename)); //Uploaded template file

            res.file_name = Some(String::from(filename));

            // File::create is blocking operation, use threadpool
            let mut f = web::block(|| std::fs::File::create(filepath))
                .await
                .unwrap();

            // Field in turn is stream of *Bytes* object
            while let Some(chunk) = field.next().await {
                let data = chunk.unwrap();
                // filesystem operations are blocking, we have to use threadpool
                f = web::block(move || f.write_all(&data).map(|_| f)).await?;
            }
        }

        let dll = env::var("DLL").expect("Please set DLL in .env");
        let platform = env::var("PLATFORM").expect("Please set PLATFORM in .env");
        
        let output = Command::new(dll)
            .args(&[res.file_name.as_ref().unwrap(), &platform.to_string()])
            .output()
            .unwrap();

        let variables_raw = String::from_utf8(output.stdout).unwrap();
        let variables_splitter = variables_raw.split(",");
        let variables_vec: Vec<&str> = variables_splitter.collect();
        let mut variables_vec_formatted: Vec<String> = Vec::new();
        for n in 0..variables_vec.len() {
            variables_vec_formatted.push(variables_vec.get(n).unwrap().to_string());
        }
        let _test_res = variables::Variables::find_by_name(Some(
            variables_vec_formatted.clone(),
        ))
        .unwrap();

        let mut formatted_variables: Vec<TemplateVariable1> = Vec::new();
        for n in 0.._test_res.len() {
            let template_var = _test_res.get(n).unwrap().clone();
            let templ_var_id = template_var.0;
            let templ_var_name = template_var.1;
            let templ_var_group = template_var.2;

            let _group = match (templ_var_group).unwrap() {
                true => Some("System".to_string()),
                false => Some("Ebys".to_string()),
            };

            let templ_var = TemplateVariable1 {
                id: Some(templ_var_id),
                variable_name: templ_var_name,
                group: _group,
                valid: Some(true),
            };

            formatted_variables.push(templ_var);
        }

        if variables_vec_formatted.len() != _test_res.len() {
            for n in 0..variables_vec_formatted.len() {
                let var_tocheck = Some(variables_vec_formatted.get(n).unwrap().to_string());
                let invalid_variable: Vec<(i32, Option<String>, Option<bool>)> = _test_res
                    .iter()
                    .filter(|&x| x.1 == var_tocheck)
                    .cloned()
                    .collect();

                if invalid_variable.len() == 0 {
                    let templ_var_invalid = TemplateVariable1 {
                        id: None,
                        variable_name: var_tocheck,
                        group: None,
                        valid: Some(false),
                    };
                    formatted_variables.push(templ_var_invalid);
                }
            }
        }

        let _template_temporary_uploaded_variables = TemplateTemporaryUploadedVariables {
            file_name: res.file_name,
            template_vars: formatted_variables,
        };

        Ok(_template_temporary_uploaded_variables)
    }
}
