use crate::templates::TemplateVariablesToSave;
use crate::templates::{Templates};
use crate::error_handler::CustomError;
use actix_web::{delete, get, post, put, web, HttpResponse,Error};
use actix_multipart::Multipart;

 #[get("/sablonlar")]
 async fn find_all() -> Result<HttpResponse, CustomError> {
     let templates = Templates::find_all()?;
    Ok(HttpResponse::Ok().json(templates))
}

 #[get("/sablonlar/{id}")]
 async fn find(id: web::Path<i32>) -> Result<HttpResponse, CustomError> {
     let template = Templates::find(id.into_inner())?;
    Ok(HttpResponse::Ok().json(template))
 }

 #[get("/sablonlargrupturbirim")]
 async fn find_group_type_unit() -> Result<HttpResponse, CustomError> {
    let response = Templates::find_group_type_unit()?;
    Ok(HttpResponse::Ok().json(response))
 }

 #[get("/sablonlarasgrouped")]
 async fn get_sablonlar_as_grouped() -> Result<HttpResponse, CustomError> {
    let response = Templates::get_sablonlar_as_grouped()?;
    Ok(HttpResponse::Ok().json(response))
 }

#[post("/sablonlar")]
async fn create(template_variables_to_save: web::Json<TemplateVariablesToSave>) -> Result<HttpResponse, Error> {
     let template = Templates::upload_template(template_variables_to_save.into_inner()).await?;
    Ok(HttpResponse::Ok().json(template))
}

#[post("/sablonlar/dosyadanokunandegiskenlerigetir")]
async fn temporary_upload(payload: Multipart) -> Result<HttpResponse, Error> {
    let template_vars = Templates::temporary_upload(payload).await?;
    Ok(HttpResponse::Ok().json(template_vars))
}

#[put("/sablonlar/{id}")]
async fn update(
    id: web::Path<i32>,
    template: web::Json<TemplateVariablesToSave>,
) -> Result<HttpResponse, CustomError> {
    let template = Templates::update_template(id.into_inner(), template.into_inner())?;
    Ok(HttpResponse::Ok().json(template))
}

#[delete("/sablonlar/{id}")]
async fn delete(id: web::Path<i32>) -> Result<HttpResponse, CustomError> {
     let template = Templates::delete(id.into_inner())?;
     Ok(HttpResponse::Ok().json(template))
 }

pub fn init_routes(config: &mut web::ServiceConfig) {
     config.service(find_all);
     config.service(find_group_type_unit);
     config.service(get_sablonlar_as_grouped);
     config.service(find);
     config.service(update);
     config.service(create);
     config.service(temporary_upload);
     config.service(delete);
}