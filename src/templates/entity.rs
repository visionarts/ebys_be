use crate::templatestatuses::TemplateStatuses;
use serde::{Deserialize, Serialize};
use chrono::{NaiveDateTime};

use crate::{documenttypes::DocumentTypes, templategroups::TemplateGroups, units::Units};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TemplateVariable1 {
    pub id: Option<i32>,
    pub variable_name: Option<String>,
    pub group: Option<String>,
    pub valid: Option<bool>
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TemplateVariableName {
    pub name: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TemplateVariableSystem {
    pub id: i32,
    pub name: String,
    pub group: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TemplateVariableByName {
    pub id: i32,
    pub template_variable_name: Option<String>,
    pub is_system_variable: Option<bool>,
    pub status: Option<bool>
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TemplatesDetailed {
    pub id: i32,
    pub template_group_id: Option<i32>,
    pub document_type_id: Option<i32>,
    pub template_status_id: Option<i32>,
    pub template_name: Option<String>,
    pub template_file_name: Option<String>,
    pub created: Option<NaiveDateTime>,
    pub updated: Option<NaiveDateTime>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TemplateTemporaryUploadedVariables {
    pub file_name: Option<String>,
    pub template_vars: Vec<TemplateVariable1>
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TemplateVariablesToSave {
    pub template_group_id: i32,
    pub document_type_id: i32,
    pub template_status_id: i32,
    pub template_name: String,
    pub template_file_name: String,
    pub units: Vec<i32>,
    pub forms : Vec<TemplateForm>,
    pub variables: Vec<i32>
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TemplateForm {
    pub form_id: Option<i32>,
    pub form_name: String,
    pub variables: Vec<i32>
}

#[derive(Serialize, Deserialize)]
pub struct GroupUnitType {
    pub template_groups: Vec<TemplateGroups>,
    pub units: Vec<Units>,
    pub document_types: Vec<DocumentTypes>
}

#[derive(Serialize, Deserialize)]
pub struct TemplatesTable {
    pub templates: Vec<Table>
}

#[derive(Serialize, Deserialize,Clone)]
pub struct Table {
    pub template_id: i32,
    pub template_name: String,
    pub template_group: TemplateGroups,
    pub template_status: TemplateStatuses,
    pub file_name: String,
    pub template_variables: Vec<TemplateVariable1>,
    pub document_type: DocumentTypes,
    pub units: Vec<Units>,
    pub forms: Vec<FormOnTemplates>
}

#[derive(Serialize, Deserialize,Clone)]
pub struct FormOnTemplates {
    pub form_id: i32,
    pub form_name: String,
    pub template_variables: Vec<TemplateVariable1>
}

#[derive(Serialize, Deserialize,Clone)]
pub struct GroupedTemplates {
    pub template_group_id: i32,
    pub template_group_name: String,
    pub templates : Vec<Table>
}