use crate::variableselectiontypes::{VariableSelectionType, VariableSelectionTypes};
use crate::error_handler::CustomError;
use actix_web::{delete, get, post, put, web, HttpResponse};
use serde_json::{json};

#[get("/sablondegiskenisecimturleri")]
async fn find_all() -> Result<HttpResponse, CustomError> {
    let template_variable_selection_types = VariableSelectionTypes::find_all()?;
    Ok(HttpResponse::Ok().json(template_variable_selection_types))
}

#[get("/sablondegiskenisecimturleri/{id}")]
async fn find(id: web::Path<i32>) -> Result<HttpResponse, CustomError> {
    let template_variable_selection_type = VariableSelectionTypes::find(id.into_inner())?;
    Ok(HttpResponse::Ok().json(template_variable_selection_type))
}

#[post("/sablondegiskenisecimturleri")]
async fn create(template_variable_selection_type: web::Json<VariableSelectionType>) -> Result<HttpResponse, CustomError> {
    let template_variable_selection_type = VariableSelectionTypes::create(template_variable_selection_type.into_inner())?;
    Ok(HttpResponse::Ok().json(template_variable_selection_type))
}

#[put("/sablondegiskenisecimturleri/{id}")]
async fn update(
    id: web::Path<i32>,
    template_variable_selection_type: web::Json<VariableSelectionType>,
) -> Result<HttpResponse, CustomError> {
    let template_variable_selection_type = VariableSelectionTypes::update(id.into_inner(), template_variable_selection_type.into_inner())?;
    Ok(HttpResponse::Ok().json(template_variable_selection_type))
}

#[delete("/sablondegiskenisecimturleri/{id}")]
async fn delete(id: web::Path<i32>) -> Result<HttpResponse, CustomError> {
    let deleted_template_variable_selection_type = VariableSelectionTypes::delete(id.into_inner())?;
    Ok(HttpResponse::Ok().json(json!({ "deleted template variable selection type": deleted_template_variable_selection_type })))
}

pub fn init_routes(comfig: &mut web::ServiceConfig) {
    comfig.service(find_all);
    comfig.service(find);
    comfig.service(create);
    comfig.service(update);
    comfig.service(delete);
}