use crate::db;
use crate::error_handler::CustomError;
use crate::schema::variableselectiontypes;
use diesel::prelude::*;
use serde::{Deserialize, Serialize};
use chrono::{NaiveDateTime};

#[derive(Serialize, Deserialize, AsChangeset, Insertable, Queryable)]
#[table_name = "variableselectiontypes"]
pub struct VariableSelectionType {
    pub selection_type_name: Option<String>,
    pub created: Option<NaiveDateTime>,
    pub updated: Option<NaiveDateTime>
}

#[derive(Serialize, Deserialize, Queryable, Insertable)]
#[table_name = "variableselectiontypes"]
pub struct VariableSelectionTypes {
    pub id: i32,
    pub selection_type_name: Option<String>,
    pub created: Option<NaiveDateTime>,
    pub updated: Option<NaiveDateTime>
}

impl VariableSelectionTypes {
    pub fn find_all() -> Result<Vec<Self>, CustomError> {
        let conn = db::connection()?;
        let variable_selection_types = variableselectiontypes::table.load::<VariableSelectionTypes>(&conn)?;
        Ok(variable_selection_types)
    }

    pub fn find(id: i32) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let template_variable_selection_type = variableselectiontypes::table.filter(variableselectiontypes::id.eq(id)).first(&conn)?;
        Ok(template_variable_selection_type)
    }

    pub fn create(template_variable_selection_type: VariableSelectionType) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let template_variable_selection_type = VariableSelectionType::from(template_variable_selection_type);
        let template_variable_selection_type = diesel::insert_into(variableselectiontypes::table)
            .values(template_variable_selection_type)
            .get_result(&conn)?;
        Ok(template_variable_selection_type)
    }

    pub fn update(id: i32, template_variable_selection_type: VariableSelectionType) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let template_variable_selection_type = diesel::update(variableselectiontypes::table)
            .filter(variableselectiontypes::id.eq(id))
            .set(template_variable_selection_type)
            .get_result(&conn)?;
        Ok(template_variable_selection_type)
    }

    pub fn delete(id: i32) -> Result<usize, CustomError> {
        let conn = db::connection()?;
        let res = diesel::delete(variableselectiontypes::table.filter(variableselectiontypes::id.eq(id))).execute(&conn)?;
        Ok(res)
    }

}

impl VariableSelectionType {
    fn from(variable_selection_type: VariableSelectionType) -> VariableSelectionType {
        VariableSelectionType {
            selection_type_name: variable_selection_type.selection_type_name,
            created: variable_selection_type.created,
            updated: variable_selection_type.updated
        }
    }
}
