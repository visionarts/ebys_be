use crate::error_handler::CustomError;
use elasticsearch::{DeleteParts, Elasticsearch, IndexParts, SearchParts, UpdateParts};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Documents {
    took: u8,
    timed_out: bool,
    _shards: Shard,
    hits: Hits,
}

#[derive(Debug, Clone, Serialize, Deserialize, Default)]
pub struct Shard {
    total: u8,
    successful: u8,
    skipped: u8,
    failed: u8,
}
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Hits {
    total: Count,
    max_score: f32,
    hits: Vec<Document>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Document {
    _index: String,
    _type: String,
    _id: String,
    _score: f32,
    _source: DocumentSource,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DocumentSource {
    document_name: String,
    document_subject: String,
    archieve_code: Option<ArchieveCode>,
    special_archieve_code: Option<ArchieveCode>,
    confidentiality: Option<Confidentiality>,
    urgency: Option<Urgency>,
    content: String,
    signers: Vec<u64>,
    individuals: Vec<u64>,
    attachments: Vec<String>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum ArchieveCode {
    AKADEMIKPERSONEL = 200,
    IDARIPERSONEL = 300,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Urgency {
    URGENT = 1,
    DAILY = 2,
    USUAL = 3,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Confidentiality {
    CONFIDENTIAL = 1,
    USUAL = 2,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Count {
    value: u8,
    relation: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct EsResponse {
    _id: String,
    _index: String,
    _type: String,
    _version: u8,
    result: String,
    _seq_no: u8,
    _primary_term: u8,
}

impl Documents {
    pub async fn find_all() -> Result<Vec<Document>, CustomError> {
        let _client = Elasticsearch::default();

        let _search_response = _client
            .search(SearchParts::Index(&["evrak"]))
            .size(1000)
            .body(json!({
                "query": {
                    "match_all": {}
                }
            }))
            .send()
            .await?;

        let json: Documents = _search_response.json().await?;
        let res: Vec<Document> = json.hits.hits;

        Ok(res)
    }

    pub async fn find(id: String) -> Result<Document, CustomError> {
        let _client = Elasticsearch::default();

        let _search_response = _client
            .search(SearchParts::Index(&["evrak"]))
            .body(json!({
                "query": {
                    "match": {"_id":id}
                }
            }))
            .allow_no_indices(true)
            .send()
            .await?;

        let json: Documents = _search_response.json().await?;

        let hits: Vec<Document> = json.hits.hits;
        let hit = &hits[0];
        let res: Document = Document::clone(hit);
        Ok(res)
    }

    pub async fn update(_document: Document) -> Result<EsResponse, CustomError> {
        let _client = Elasticsearch::default();
        let _update_response = _client
            .update(UpdateParts::IndexId(&_document._index, &_document._id))
            .body(json!({
                "doc": {
                    "document_name": &_document._source.document_name,
                    "document_subject": &_document._source.document_subject,
                    "archieve_code": &_document._source.archieve_code,
                    "special_archieve_code": &_document._source.special_archieve_code,
                    "confidentiality": &_document._source.confidentiality,
                    "urgency": &_document._source.urgency,
                    "content": &_document._source.content,
                    "signers": &_document._source.signers,
                    "individuals": &_document._source.individuals,
                    "attachments": &_document._source.attachments
                }
            }))
            .send()
            .await?;

        let json = _update_response.json::<Value>().await?;
        let res: EsResponse = serde_json::from_value(json).unwrap();

        Ok(res)
    }

    pub async fn create(_document: DocumentSource) -> Result<EsResponse, CustomError> {
        let _client = Elasticsearch::default();
        let _create_response = _client
            .index(IndexParts::Index("evrak"))
            .body(json!({
                "document_name": &_document.document_name,
                "document_subject": &_document.document_subject,
                "archieve_code": &_document.archieve_code,
                "special_archieve_code": &_document.special_archieve_code,
                "confidentiality": &_document.confidentiality,
                "urgency": &_document.urgency,
                "content": &_document.content,
                "signers": &_document.signers,
                "individuals": &_document.individuals,
                "attachments": &_document.attachments
            }))
            .send()
            .await?;

            let json = _create_response.json::<Value>().await?;
            let res: EsResponse = serde_json::from_value(json).unwrap();
            // let res1 = Command::new("C://ebys//ebys_be//external//publish//ebys_doc.exe").output().expect("fuk");
            Ok(res)
    }

    // pub async fn create(mut payload: Multipart) -> Result<HttpResponse, Error> {
    //     // iterate over multipart stream
    //     while let Ok(Some(mut field)) = payload.try_next().await {
    //         let content_type = field.content_disposition().unwrap();
    //         let filename = content_type.get_filename().unwrap();
    //         let filepath = format!("./tmp/{}", sanitize_filename::sanitize(&filename));

    //         // File::create is blocking operation, use threadpool
    //         let mut f = web::block(|| std::fs::File::create(filepath))
    //             .await
    //             .unwrap();

    //         // Field in turn is stream of *Bytes* object
    //         while let Some(chunk) = field.next().await {
    //             let data = chunk.unwrap();
    //             // filesystem operations are blocking, we have to use threadpool
    //             f = web::block(move || f.write_all(&data).map(|_| f)).await?;
    //         }
    //     }
    //     Ok(HttpResponse::Ok().into())
    // }

    pub async fn delete(id: String) -> Result<EsResponse, CustomError> {
        let _client = Elasticsearch::default();
        let _update_response = _client
            .delete(DeleteParts::IndexId("evrak", &id))
            .send()
            .await?;

        let json = _update_response.json::<Value>().await?;
        let res: EsResponse = serde_json::from_value(json).unwrap();

        Ok(res)
    }
}
