use crate::documents::{Documents, Document, DocumentSource};
use crate::error_handler::CustomError;
use actix_web::{get, post, put, delete, web, HttpResponse};

#[get("/documents")]
async fn find_all() -> Result<HttpResponse, CustomError> {
    let documents = Documents::find_all().await?;
    Ok(HttpResponse::Ok().json(documents))
}

#[get("/documents/{id}")]
async fn find(id: web::Path<String>) -> Result<HttpResponse, CustomError> {
    let document = Documents::find(id.to_string()).await?;
    Ok(HttpResponse::Ok().json(document))
}

#[put("/documents")]
async fn update(_document: web::Json<Document>) -> Result<HttpResponse, CustomError> {
    let document = Documents::update(_document.into_inner()).await?;
    Ok(HttpResponse::Ok().json(document))
}

#[post("/documents")]
async fn create(_document: web::Json<DocumentSource>) -> Result<HttpResponse, CustomError> {
    let document = Documents::create(_document.into_inner()).await?;
    Ok(HttpResponse::Ok().json(document))
}

// #[post("/documents")]
// async fn create(payload: Multipart) -> Result<HttpResponse, Error> {
//     let document = Documents::create(payload).await?;
//     Ok(HttpResponse::Ok().into())

// }

#[delete("/documents/{id}")]
async fn delete(id: web::Path<String>) -> Result<HttpResponse, CustomError> {
    let document = Documents::delete(id.to_string()).await?;
    Ok(HttpResponse::Ok().json(document))
}

pub fn init_routes(config: &mut web::ServiceConfig) {
    config.service(find_all);
    config.service(find);
    config.service(update);
    config.service(create);
    config.service(delete);
}
