şablon oluştur tablosunun datası

[
    {
        sablon_id: ...,
        sanlon_name: ...,
        sablon_group: {
            id: 1,
            template_group_name: "Standart Evrak",
            adding_date_time: "2015-10-26T13:44:09.507",
        },
        document_type: {
            id: 1,
            document_type_name: "Normal Evrak",
            is_physical_document: false
        },
        units : [
            {
            id: 1,
            unit_name: "KURULLAR VE KOMİSYONLAR",
            visible_unit_name: "Kurullar Ve Komisyonlar",
            main_foundation_administrative_identification_code: 99132376,
            detsis_no: 11535452,
            foundation_unit_type_id1: 134,
            foundation_unit_type_id2: 186,
            hierarchical_foundation: "SÜLEYMAN DEMİREL ÜNİVERSİTESİ REKTÖRLÜĞÜ>SU ENSTİTÜSÜ MÜDÜRLÜĞÜ>KURULLAR VE KOMİSYONLAR",
            parent_administrative_identification_code: 35978225,
            parent_administrative_identification_code2: 0,
            telephone: null,
            fax: null,
            address: null,
            email: "iibf@sdu.edu.tr",
            internet_address: null,
            visible_hierarchy_in_documents: "Su Enstitüsü Müdürlüğü<br/>Kurullar Ve Komisyonlar",
            little_hierarchical_foundation: "Süleyman Demirel Üniversitesi Rektörlüğü>Su Enstitüsü Müdürlüğü>Kurullar Ve Komisyonlar",
            is_active: false,
            address_information: null
            },
            {
                id: ...,
                name: ...
            },
        ],
        template_status: {
            id: ...,
            name: ...
        },
        file_name: "genelyazismasablonu.docx",
        template_variables: [
            {
                "id": 1,
                "variable_name": "YAZI",
                "group": "Ebys",
                "valid": true
            },
            {
                "id": 2,
                "variable_name": "KONU",
                "group": "Ebys",
                "valid": true
            }
        ],
        forms: [
            {
                id: ...,
                "form_name": "FORM 1",
                "template_variables": [
                    {
                        "id": 1,
                        "variable_name": "YAZI",
                        "group": "Ebys",
                        "valid": true
                    },
                    {
                    "id": 2,
                    "variable_name": "KONU",
                    "group": "Ebys",
                    "valid": true
                    }
                ]
            },
            {
                id: ...,
                "form_name": "FORM 2",
                "template_variables": [
                    {
                        "id": 1,
                        "variable_name": "YAZI",
                        "group": "Ebys",
                        "valid": true
                    },
                    {
                        "id": 2,
                        "variable_name": "KONU",
                        "group": "Ebys",
                        "valid": true
                    }
                ]
            }
      ]
    }
]


şablonu keydet
unit arrayini unutmuşuz
