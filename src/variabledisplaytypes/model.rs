use crate::db;
use crate::error_handler::CustomError;
use crate::schema::variabledisplaytypes;
use diesel::prelude::*;
use serde::{Deserialize, Serialize};
use chrono::{NaiveDateTime};

#[derive(Serialize, Deserialize, AsChangeset, Insertable, Queryable)]
#[table_name = "variabledisplaytypes"]
pub struct VariableDisplayType {
    pub display_type_name: Option<String>,
    pub created: Option<NaiveDateTime>,
    pub updated: Option<NaiveDateTime>
}

#[derive(Serialize, Deserialize, Queryable, Insertable)]
#[table_name = "variabledisplaytypes"]
pub struct VariableDisplayTypes {
    pub id: i32,
    pub display_type_name: Option<String>,
    pub created: Option<NaiveDateTime>,
    pub updated: Option<NaiveDateTime>
}

impl VariableDisplayTypes {
    pub fn find_all() -> Result<Vec<Self>, CustomError> {
        let conn = db::connection()?;
        let template_variable_display_types = variabledisplaytypes::table.load::<VariableDisplayTypes>(&conn)?;
        Ok(template_variable_display_types)
    }

    pub fn find(id: i32) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let template_variable_display_type = variabledisplaytypes::table.filter(variabledisplaytypes::id.eq(id)).first(&conn)?;
        Ok(template_variable_display_type)
    }

    pub fn create(template_variable_display_type: VariableDisplayType) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let template_variable_display_type = VariableDisplayType::from(template_variable_display_type);
        let template_variable_display_type = diesel::insert_into(variabledisplaytypes::table)
            .values(template_variable_display_type)
            .get_result(&conn)?;
        Ok(template_variable_display_type)
    }

    pub fn update(id: i32, template_variable_display_type: VariableDisplayType) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let template_variable_display_type = diesel::update(variabledisplaytypes::table)
            .filter(variabledisplaytypes::id.eq(id))
            .set(template_variable_display_type)
            .get_result(&conn)?;
        Ok(template_variable_display_type)
    }

    pub fn delete(id: i32) -> Result<usize, CustomError> {
        let conn = db::connection()?;
        let res = diesel::delete(variabledisplaytypes::table.filter(variabledisplaytypes::id.eq(id))).execute(&conn)?;
        Ok(res)
    }

}

impl VariableDisplayType {
    fn from(variable_display_type: VariableDisplayType) -> VariableDisplayType {
        VariableDisplayType {
            display_type_name: variable_display_type.display_type_name,
            created: variable_display_type.created,
            updated: variable_display_type.updated
        }
    }
}
