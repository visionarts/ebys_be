use crate::variabledisplaytypes::{VariableDisplayType, VariableDisplayTypes};
use crate::error_handler::CustomError;
use actix_web::{delete, get, post, put, web, HttpResponse};
use serde_json::{json};

#[get("/sablondegiskenigoruntulemeturleri")]
async fn find_all() -> Result<HttpResponse, CustomError> {
    let template_variable_display_types = VariableDisplayTypes::find_all()?;
    Ok(HttpResponse::Ok().json(template_variable_display_types))
}

#[get("/sablondegiskenigoruntulemeturleri/{id}")]
async fn find(id: web::Path<i32>) -> Result<HttpResponse, CustomError> {
    let template_variable_display_type = VariableDisplayTypes::find(id.into_inner())?;
    Ok(HttpResponse::Ok().json(template_variable_display_type))
}

#[post("/sablondegiskenigoruntulemeturleri")]
async fn create(template_variable_display_type: web::Json<VariableDisplayType>) -> Result<HttpResponse, CustomError> {
    let template_variable_display_type = VariableDisplayTypes::create(template_variable_display_type.into_inner())?;
    Ok(HttpResponse::Ok().json(template_variable_display_type))
}

#[put("/sablondegiskenigoruntulemeturleri/{id}")]
async fn update(
    id: web::Path<i32>,
    template_variable_display_type: web::Json<VariableDisplayType>,
) -> Result<HttpResponse, CustomError> {
    let template_variable_display_type = VariableDisplayTypes::update(id.into_inner(), template_variable_display_type.into_inner())?;
    Ok(HttpResponse::Ok().json(template_variable_display_type))
}

#[delete("/sablondegiskenigoruntulemeturleri/{id}")]
async fn delete(id: web::Path<i32>) -> Result<HttpResponse, CustomError> {
    let deleted_template_variable_display_type = VariableDisplayTypes::delete(id.into_inner())?;
    Ok(HttpResponse::Ok().json(json!({ "deleted template variable display type": deleted_template_variable_display_type })))
}

pub fn init_routes(comfig: &mut web::ServiceConfig) {
    comfig.service(find_all);
    comfig.service(find);
    comfig.service(create);
    comfig.service(update);
    comfig.service(delete);
}