use crate::templatevariables::{TemplateVariable, TemplateVariables};
use crate::error_handler::CustomError;
use actix_web::{delete, get, post, put, web, HttpResponse};
use serde_json::{json};

#[get("/sablonsablondegiskenleri")]
async fn find_all() -> Result<HttpResponse, CustomError> {
    let template_to_template_variables = TemplateVariables::find_all()?;
    Ok(HttpResponse::Ok().json(template_to_template_variables))
}

#[get("/sablonsablondegiskenleri/{id}")]
async fn find(id: web::Path<i32>) -> Result<HttpResponse, CustomError> {
    let template_to_template_variable = TemplateVariables::find(id.into_inner())?;
    Ok(HttpResponse::Ok().json(template_to_template_variable))
}

#[post("/sablonsablondegiskenleri")]
async fn create(template_to_template_variable: web::Json<TemplateVariable>) -> Result<HttpResponse, CustomError> {
    let template_to_template_variable = TemplateVariables::create(template_to_template_variable.into_inner())?;
    Ok(HttpResponse::Ok().json(template_to_template_variable))
}

#[put("/sablonsablondegiskenleri/{id}")]
async fn update(
    id: web::Path<i32>,
    template_to_template_variable: web::Json<TemplateVariable>,
) -> Result<HttpResponse, CustomError> {
    let template_to_template_variable = TemplateVariables::update(id.into_inner(), template_to_template_variable.into_inner())?;
    Ok(HttpResponse::Ok().json(template_to_template_variable))
}

#[delete("/sablonsablondegiskenleri/{id}")]
async fn delete(id: web::Path<i32>) -> Result<HttpResponse, CustomError> {
    let deleted_template_to_template_variable = TemplateVariables::delete(id.into_inner())?;
    Ok(HttpResponse::Ok().json(json!({ "deleted template to template variable": deleted_template_to_template_variable })))
}

pub fn init_routes(comfig: &mut web::ServiceConfig) {
    comfig.service(find_all);
    comfig.service(find);
    comfig.service(create);
    comfig.service(update);
    comfig.service(delete);
}