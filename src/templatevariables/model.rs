use crate::templates::TemplateVariable1;
use crate::forms::Forms;
use crate::variables::Variables;
use crate::schema::templatevariables::dsl::*;
use crate::db;
use crate::error_handler::CustomError;
use crate::schema::templatevariables;
use crate::schema::forms;
use crate::schema::variables;
use diesel::prelude::*;
use serde::{Deserialize, Serialize};
use chrono::{NaiveDateTime};

#[derive(Serialize, Deserialize, AsChangeset, Insertable, Queryable, Clone)]
#[table_name = "templatevariables"]
pub struct TemplateVariable {
    pub template_id: i32,
    pub variable_id: i32,
    pub form_id: i32,
    pub created: Option<NaiveDateTime>,
    pub updated: Option<NaiveDateTime>
}

#[derive(Serialize, Deserialize, Queryable, Insertable, Clone)]
#[table_name = "templatevariables"]
pub struct TemplateVariables {
    pub id: i32,
    pub template_id: i32,
    pub variable_id: i32,
    pub form_id: i32,
    pub created: Option<NaiveDateTime>,
    pub updated: Option<NaiveDateTime>
}

impl TemplateVariables {
    pub fn find_all() -> Result<Vec<Self>, CustomError> {
        let conn = db::connection()?;
        let template_variables = templatevariables::table.load::<TemplateVariables>(&conn)?;
        Ok(template_variables)
    }

    pub fn get_variables_by_formid(_form_ids: i32) -> Result<Vec<TemplateVariable1>, CustomError> {
        let conn = db::connection()?;
        let variable_ids: Vec<i32> = templatevariables::table.filter(templatevariables::form_id.eq(_form_ids)).select(templatevariables::variable_id).get_results::<i32>(&conn)?;    
        let _variables_temp: Vec<Variables> = variables::table.filter(variables::id.eq_any(variable_ids)).load(&conn)?;
        let mut _variables: Vec<TemplateVariable1> = Vec::new();

        for n in 0.._variables_temp.len() {
            let template_var = _variables_temp.get(n).unwrap().clone();
            let templ_var_id = template_var.id;
            let templ_var_name = template_var.variable_name;
            let templ_var_group = template_var.is_system_variable;

            let _group = match (templ_var_group).unwrap() {
                true => Some("System".to_string()),
                false => Some("Ebys".to_string()),
            };

            let templ_var = TemplateVariable1 {
                id: Some(templ_var_id),
                variable_name: templ_var_name,
                group: _group,
                valid: Some(true),
            };
            _variables.push(templ_var);
        }


        Ok(_variables)
    }

    pub fn get_variables_by_templateid(_template_id: i32) -> Result<Vec<TemplateVariable1>, CustomError> {
        let conn = db::connection()?;
        let variable_ids: Vec<i32> = templatevariables::table.filter(templatevariables::template_id.eq(_template_id)).select(templatevariables::variable_id).get_results::<i32>(&conn)?;    
        let _variables_temp: Vec<Variables> = variables::table.filter(variables::id.eq_any(variable_ids)).load(&conn)?;
        let mut _variables: Vec<TemplateVariable1> = Vec::new();

        for n in 0.._variables_temp.len() {
            let template_var = _variables_temp.get(n).unwrap().clone();
            let templ_var_id = template_var.id;
            let templ_var_name = template_var.variable_name;
            let templ_var_group = template_var.is_system_variable;

            let _group = match (templ_var_group).unwrap() {
                true => Some("System".to_string()),
                false => Some("Ebys".to_string()),
            };

            let templ_var = TemplateVariable1 {
                id: Some(templ_var_id),
                variable_name: templ_var_name,
                group: _group,
                valid: Some(true),
            };
            _variables.push(templ_var);
        }


        Ok(_variables)
    }

    pub fn get_forms_by_templateid(_template_id: i32) -> Result<Vec<Forms>, CustomError> {
        let conn = db::connection()?;
        let form_ids: Vec<i32> = templatevariables::table.filter(templatevariables::template_id.eq(_template_id)).select(templatevariables::form_id).get_results::<i32>(&conn)?;    
        let _forms: Vec<Forms> = forms::table.filter(forms::id.eq_any(form_ids)).load(&conn)?;

        Ok(_forms)
    }

    pub fn find(_id: i32) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let template_to_template_variable = templatevariables::table.filter(templatevariables::id.eq(_id)).first(&conn)?;
        Ok(template_to_template_variable)
    }

    pub fn create(template_to_template_variable: TemplateVariable) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let template_to_template_variable = TemplateVariable::from(template_to_template_variable);
        let template_to_template_variable = diesel::insert_into(templatevariables::table)
            .values(template_to_template_variable)
            .get_result(&conn)?;
        Ok(template_to_template_variable)
    }

    pub fn create_bulk(template_to_template_variables: Vec<TemplateVariable>) -> Result<Vec<i32>, CustomError> {
        let conn = db::connection()?;

        let inserted_vars: Vec<i32> = diesel::insert_into(templatevariables)
            .values(&template_to_template_variables)
            .returning(id)
            .get_results(&conn)?;

        Ok(inserted_vars)
    }

    pub fn update(_id: i32, template_to_template_variable: TemplateVariable) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let template_to_template_variable = diesel::update(templatevariables::table)
            .filter(templatevariables::id.eq(_id))
            .set(template_to_template_variable)
            .get_result(&conn)?;
        Ok(template_to_template_variable)
    }

    pub fn delete(_id: i32) -> Result<usize, CustomError> {
        let conn = db::connection()?;
        let res = diesel::delete(templatevariables::table.filter(templatevariables::id.eq(_id))).execute(&conn)?;
        Ok(res)
    }

    pub fn delete_by_template_id(_template_id: i32) -> Result<usize, CustomError> {
        let conn = db::connection()?;
        let res = diesel::delete(templatevariables::table.filter(templatevariables::template_id.eq(_template_id))).execute(&conn)?;
        Ok(res)
    }

}

impl TemplateVariable {
    fn from(template_variable: TemplateVariable) -> TemplateVariable {
        TemplateVariable {
            template_id: template_variable.template_id,
            variable_id: template_variable.variable_id,
            form_id: template_variable.form_id,
            created: template_variable.created,
            updated: template_variable.updated
        }
    }
}
