use crate::documenttypes::{DocumentType, DocumentTypes};
use crate::error_handler::CustomError;
use actix_web::{delete, get, post, put, web, HttpResponse};
use serde_json::{json};

#[get("/evraktur")]
async fn find_all() -> Result<HttpResponse, CustomError> {
    let document_types = DocumentTypes::find_all()?;
    Ok(HttpResponse::Ok().json(document_types))
}

#[get("/evraktur/{id}")]
async fn find(id: web::Path<i32>) -> Result<HttpResponse, CustomError> {
    let document_type = DocumentTypes::find(id.into_inner())?;
    Ok(HttpResponse::Ok().json(document_type))
}

#[post("/evraktur")]
async fn create(document_type: web::Json<DocumentType>) -> Result<HttpResponse, CustomError> {
    let document_type = DocumentTypes::create(document_type.into_inner())?;
    Ok(HttpResponse::Ok().json(document_type))
}

#[put("/evraktur/{id}")]
async fn update(
    id: web::Path<i32>,
    document_type: web::Json<DocumentType>,
) -> Result<HttpResponse, CustomError> {
    let document_type = DocumentTypes::update(id.into_inner(), document_type.into_inner())?;
    Ok(HttpResponse::Ok().json(document_type))
}

#[delete("/evraktur/{id}")]
async fn delete(id: web::Path<i32>) -> Result<HttpResponse, CustomError> {
    let deleted_document_type = DocumentTypes::delete(id.into_inner())?;
    Ok(HttpResponse::Ok().json(json!({ "deleted document type": deleted_document_type })))
}

pub fn init_routes(config: &mut web::ServiceConfig) {
    config.service(find_all);
    config.service(find);
    config.service(create);
    config.service(update);
    config.service(delete);
}