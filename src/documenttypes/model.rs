use crate::db;
use crate::error_handler::CustomError;
use crate::schema::documenttypes;
use diesel::prelude::*;
use serde::{Deserialize, Serialize};
use chrono::{NaiveDateTime}; 

#[derive(Serialize, Deserialize, AsChangeset, Insertable, Queryable,Clone)]
#[table_name = "documenttypes"]
pub struct DocumentType {
    pub document_type: Option<String>,
    pub is_physical_document: Option<bool>,
    pub created: Option<NaiveDateTime>, 
    pub updated: Option<NaiveDateTime> 
}

#[derive(Serialize, Deserialize, Queryable, Insertable,Clone)]
#[table_name = "documenttypes"]
pub struct DocumentTypes {
    pub id: i32,
    pub document_type: Option<String>,
    pub is_physical_document: Option<bool>,
    pub created: Option<NaiveDateTime>, 
    pub updated: Option<NaiveDateTime> 
}

impl DocumentTypes {
    pub fn find_all() -> Result<Vec<Self>, CustomError> {
        let conn = db::connection()?;
        let document_types = documenttypes::table.load::<DocumentTypes>(&conn)?;
        Ok(document_types)
    }

    pub fn find(id: i32) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let document_type = documenttypes::table.filter(documenttypes::id.eq(id)).first(&conn)?;
        Ok(document_type)
    }

    pub fn create(document_type: DocumentType) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let document_type = DocumentType::from(document_type);
        let document_type = diesel::insert_into(documenttypes::table)
            .values(document_type)
            .get_result(&conn)?;
        Ok(document_type)
    }

    pub fn update(id: i32, document_type: DocumentType) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let document_type = diesel::update(documenttypes::table)
            .filter(documenttypes::id.eq(id))
            .set(document_type)
            .get_result(&conn)?;
        Ok(document_type)
    }

    pub fn delete(id: i32) -> Result<usize, CustomError> {
        let conn = db::connection()?;
        let res = diesel::delete(documenttypes::table.filter(documenttypes::id.eq(id))).execute(&conn)?;
        Ok(res)
    }

}

impl DocumentType {
    fn from(document_type: DocumentType) -> DocumentType {
        DocumentType {
            document_type: document_type.document_type,
            is_physical_document: document_type.is_physical_document,
            created: document_type.created,
            updated: document_type.updated
        }
    }
}