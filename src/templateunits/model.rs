use crate::units::Units;
use crate::schema::templateunits::dsl::*;
use crate::db;
use crate::error_handler::CustomError;
use crate::schema::templateunits;
use crate::schema::units;
use diesel::prelude::*;
use serde::{Deserialize, Serialize};
use chrono::{NaiveDateTime};

#[derive(Debug,Serialize, Deserialize, AsChangeset, Insertable, Queryable)]
#[table_name = "templateunits"]
pub struct TemplateUnit {
    pub unit_id: i32,
    pub template_id: i32,
    pub created: Option<NaiveDateTime>, 
    pub updated: Option<NaiveDateTime> 
}

#[derive(Debug,Serialize, Deserialize, Queryable, Insertable)]
#[table_name = "templateunits"]
pub struct TemplateUnits {
    pub id: i32,
    pub unit_id: i32,
    pub template_id: i32,
    pub created: Option<NaiveDateTime>, 
    pub updated: Option<NaiveDateTime> 
}

impl TemplateUnits {
    pub fn find_all() -> Result<Vec<Self>, CustomError> {
        let conn = db::connection()?;
        let template_units = templateunits::table.load::<TemplateUnits>(&conn)?;
        Ok(template_units)
    }

    pub fn find(_id: i32) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let template_unit = templateunits::table.filter(templateunits::id.eq(_id)).first(&conn)?;
        Ok(template_unit)
    }

    pub fn find_by_templateid(_template_id: i32) -> Result<Vec<Units>, CustomError> {
        let conn = db::connection()?;
        let unit_ids: Vec<i32> = templateunits::table.filter(templateunits::template_id.eq(_template_id)).select(templateunits::unit_id).get_results::<i32>(&conn)?;    
        let _units: Vec<Units> = units::table.filter(units::id.eq_any(unit_ids)).load(&conn)?;

        Ok(_units)
    }

    pub fn create(_template_unit: TemplateUnit) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let template_unit = TemplateUnit::from(_template_unit);
        let template_unit = diesel::insert_into(templateunits::table)
            .values(template_unit)
            .get_result(&conn)?;
        Ok(template_unit)
    }

    pub fn create_bulk(_template_units: Vec<TemplateUnit>) -> Result<Vec<i32>, CustomError> {
        let conn = db::connection()?;

        let inserted_units: Vec<i32> = diesel::insert_into(templateunits)
            .values(&_template_units)
            .returning(id)
            .get_results(&conn)?;

        Ok(inserted_units)
    }

    pub fn update(_id: i32, template_unit: TemplateUnit) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let template_unit = diesel::update(templateunits::table)
            .filter(templateunits::id.eq(_id))
            .set(template_unit)
            .get_result(&conn)?;
        Ok(template_unit)
    }

    pub fn delete(_id: i32) -> Result<usize, CustomError> {
        let conn = db::connection()?;
        let res = diesel::delete(templateunits::table.filter(templateunits::id.eq(_id))).execute(&conn)?;
        Ok(res)
    }

    pub fn delete_by_template_id(_template_id: i32) -> Result<usize, CustomError> {
        let conn = db::connection()?;
        let res = diesel::delete(templateunits::table.filter(templateunits::template_id.eq(_template_id))).execute(&conn)?;
        Ok(res)
    }

}

impl TemplateUnit {
    fn from(template_unit: TemplateUnit) -> TemplateUnit {
        TemplateUnit {
            unit_id: template_unit.unit_id,
            template_id: template_unit.template_id,
            created: template_unit.created,
            updated: template_unit.updated
        }
    }
}