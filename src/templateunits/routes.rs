use crate::templateunits::{TemplateUnit, TemplateUnits};
use crate::error_handler::CustomError;
use actix_web::{delete, get, post, put, web, HttpResponse};
use serde_json::{json};

#[get("/templateunits")]
async fn find_all() -> Result<HttpResponse, CustomError> {
    let template_units = TemplateUnits::find_all()?;
    Ok(HttpResponse::Ok().json(template_units))
}

#[get("/templateunits/{id}")]
async fn find(id: web::Path<i32>) -> Result<HttpResponse, CustomError> {
    let template_unit = TemplateUnits::find(id.into_inner())?;
    Ok(HttpResponse::Ok().json(template_unit))
}

#[post("/templateunits")]
async fn create(template_unit: web::Json<TemplateUnit>) -> Result<HttpResponse, CustomError> {
    let template_unit = TemplateUnits::create(template_unit.into_inner())?;
    Ok(HttpResponse::Ok().json(template_unit))
}

#[put("/templateunits/{id}")]
async fn update(
    id: web::Path<i32>,
    template_unit: web::Json<TemplateUnit>,
) -> Result<HttpResponse, CustomError> {
    let template_unit = TemplateUnits::update(id.into_inner(), template_unit.into_inner())?;
    Ok(HttpResponse::Ok().json(template_unit))
}

#[delete("/templateunits/{id}")]
async fn delete(id: web::Path<i32>) -> Result<HttpResponse, CustomError> {
    let deleted_template_unit = TemplateUnits::delete(id.into_inner())?;
    Ok(HttpResponse::Ok().json(json!({ "deleted template unit": deleted_template_unit })))
}

pub fn init_routes(config: &mut web::ServiceConfig) {
    config.service(find_all);
    config.service(find);
    config.service(create);
    config.service(update);
    config.service(delete);
}