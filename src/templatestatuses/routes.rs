use crate::templatestatuses::{TemplateStatus, TemplateStatuses};
use crate::error_handler::CustomError;
use actix_web::{delete, get, post, put, web, HttpResponse};
use serde_json::{json};

#[get("/sablondurumlari")]
async fn find_all() -> Result<HttpResponse, CustomError> {
    let template_statuses = TemplateStatuses::find_all()?;
    Ok(HttpResponse::Ok().json(template_statuses))
}

#[get("/sablondurumlari/{id}")]
async fn find(id: web::Path<i32>) -> Result<HttpResponse, CustomError> {
    let template_status = TemplateStatuses::find(id.into_inner())?;
    Ok(HttpResponse::Ok().json(template_status))
}

#[post("/sablondurumlari")]
async fn create(template_status: web::Json<TemplateStatus>) -> Result<HttpResponse, CustomError> {
    let template_status = TemplateStatuses::create(template_status.into_inner())?;
    Ok(HttpResponse::Ok().json(template_status))
}

#[put("/sablondurumlari/{id}")]
async fn update(
    id: web::Path<i32>,
    template_status: web::Json<TemplateStatus>,
) -> Result<HttpResponse, CustomError> {
    let template_status = TemplateStatuses::update(id.into_inner(), template_status.into_inner())?;
    Ok(HttpResponse::Ok().json(template_status))
}

#[delete("/sablondurumlari/{id}")]
async fn delete(id: web::Path<i32>) -> Result<HttpResponse, CustomError> {
    let deleted_template_status = TemplateStatuses::delete(id.into_inner())?;
    Ok(HttpResponse::Ok().json(json!({ "deleted template status": deleted_template_status })))
}

pub fn init_routes(comfig: &mut web::ServiceConfig) {
    comfig.service(find_all);
    comfig.service(find);
    comfig.service(create);
    comfig.service(update);
    comfig.service(delete);
}