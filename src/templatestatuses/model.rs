use crate::db;
use crate::error_handler::CustomError;
use crate::schema::templatestatuses;
use diesel::prelude::*;
use serde::{Deserialize, Serialize};
use chrono::{NaiveDateTime};

#[derive(Serialize, Deserialize, AsChangeset, Insertable, Queryable,Clone)]
#[table_name = "templatestatuses"]
pub struct TemplateStatus {
    pub status_name: Option<String>,
    pub created: Option<NaiveDateTime>,
    pub updated: Option<NaiveDateTime>
}

#[derive(Serialize, Deserialize, Queryable, Insertable,Clone)]
#[table_name = "templatestatuses"]
pub struct TemplateStatuses {
    pub id: i32,
    pub status_name: Option<String>,
    pub created: Option<NaiveDateTime>,
    pub updated: Option<NaiveDateTime>
}

impl TemplateStatuses {
    pub fn find_all() -> Result<Vec<Self>, CustomError> {
        let conn = db::connection()?;
        let template_statuses = templatestatuses::table.load::<TemplateStatuses>(&conn)?;
        Ok(template_statuses)
    }

    pub fn find(id: i32) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let template_status = templatestatuses::table.filter(templatestatuses::id.eq(id)).first(&conn)?;
        Ok(template_status)
    }

    pub fn create(template_status: TemplateStatus) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let template_status = TemplateStatus::from(template_status);
        let template_status = diesel::insert_into(templatestatuses::table)
            .values(template_status)
            .get_result(&conn)?;
        Ok(template_status)
    }

    pub fn update(id: i32, template_status: TemplateStatus) -> Result<Self, CustomError> {
        let conn = db::connection()?;
        let template_status = diesel::update(templatestatuses::table)
            .filter(templatestatuses::id.eq(id))
            .set(template_status)
            .get_result(&conn)?;
        Ok(template_status)
    }

    pub fn delete(id: i32) -> Result<usize, CustomError> {
        let conn = db::connection()?;
        let res = diesel::delete(templatestatuses::table.filter(templatestatuses::id.eq(id))).execute(&conn)?;
        Ok(res)
    }

}

impl TemplateStatus {
    fn from(template_status: TemplateStatus) -> TemplateStatus {
        TemplateStatus {
            status_name: template_status.status_name,
            created: template_status.created,
            updated: template_status.updated
        }
    }
}
