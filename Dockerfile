# select image
FROM rust:1.41

# copy your source tree
COPY ./ ./

ENV RUST_LOG=rest_api=info,actix=info,diesel_migrations=info
ENV DATABASE_URL=postgres://postgres:1@host.docker.internal/ebys_test
ENV HOST=0.0.0.0
ENV PORT=5000
ENV RUST_BACKTRACE=1


# build for release
RUN cargo build --release

# set the startup command to run your binary
CMD ["./target/release/ebys_be","--host", "0.0.0.0"]


