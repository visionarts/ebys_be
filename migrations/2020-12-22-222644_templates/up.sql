-- Your SQL goes here
CREATE TABLE templates (
  template_id SERIAL PRIMARY KEY,
  template_group_id INTEGER REFERENCES templategroups,
  document_type_id INTEGER REFERENCES documenttypes,
  template_status_id INTEGER REFERENCES templatestatuses,
  template_name VARCHAR,
  template_file_name VARCHAR,
  template_vars TEXT[],
  created TIMESTAMP WITH TIME ZONE,
  updated TIMESTAMP WITH TIME ZONE
);

--triggers
CREATE TRIGGER update_templates_modtime BEFORE INSERT OR UPDATE ON templates FOR EACH ROW EXECUTE PROCEDURE  update_modified_column();


-- INSERT INTO public.templates( template_group_id, document_type_id, template_status_id, template_name, template_word_file_name) VALUES (124,1,2,'Düz Evrak',true,'Yeni Kararname.doc');
-- INSERT INTO public.templates( template_group_id, document_type_id, template_status_id, template_name, template_word_file_name) VALUES (124,4,3,'Şahıs Evrakları',true,'Şahıs Evrakları.docx.docx');
-- INSERT INTO public.templates( template_group_id, document_type_id, template_status_id, template_name, template_word_file_name) VALUES (140,6,1,'Dilekçe',true,'Dilekçe.docx');
-- INSERT INTO public.templates( template_group_id, document_type_id, template_status_id, template_name, template_word_file_name) VALUES (127,5,3,'Yeni Şablon2',true,'Gorevlendirme1.doc');
-- INSERT INTO public.templates( template_group_id, document_type_id, template_status_id, template_name, template_word_file_name) VALUES (143,1,1,'Genel Yazışma Şablonu',true,'Genel Yazışma Şablonu.docx');
-- INSERT INTO public.templates( template_group_id, document_type_id, template_status_id, template_name, template_word_file_name) VALUES (143,1,1,'Adalet MYO Genel Yazışma Şablonu',false,'amblem.docx');
-- INSERT INTO public.templates( template_group_id, document_type_id, template_status_id, template_name, template_word_file_name) VALUES (143,1,3,'Elektronik Posta Şablonu',false,'Elektronik Posta Şablonu.docx');
-- INSERT INTO public.templates( template_group_id, document_type_id, template_status_id, template_name, template_word_file_name) VALUES (143,1,3,'Yazışma Şablonu Açıklama Alanlı',true,'Yazışma Şablonu Açıklama Alanlı.docx');
-- INSERT INTO public.templates( template_group_id, document_type_id, template_status_id, template_name, template_word_file_name) VALUES (128,13,1,'Olur Şablonu',true,'Genel Yazışma Şablonu.docx');
-- INSERT INTO public.templates( template_group_id, document_type_id, template_status_id, template_name, template_word_file_name) VALUES (127,1,2,'Dialog Şablon',true,'Makale.docx');