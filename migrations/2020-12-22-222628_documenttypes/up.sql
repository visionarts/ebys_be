-- Your SQL goes here

CREATE TABLE "documenttypes" (
  id SERIAL PRIMARY KEY,
  document_type VARCHAR,
  is_physical_document BOOLEAN,
  created TIMESTAMP WITH TIME ZONE,
  updated TIMESTAMP WITH TIME ZONE
);

CREATE TRIGGER update_variables_modtime BEFORE INSERT OR UPDATE ON documenttypes FOR EACH ROW EXECUTE PROCEDURE  update_modified_column();

INSERT INTO public.documenttypes(document_type, is_physical_document) VALUES ('Normal Evrak',false);
INSERT INTO public.documenttypes(document_type, is_physical_document) VALUES ('Şahıs Evrak',false);
INSERT INTO public.documenttypes(document_type, is_physical_document) VALUES ('Yazı',false);
INSERT INTO public.documenttypes(document_type, is_physical_document) VALUES ('Dilekçe',false);
INSERT INTO public.documenttypes(document_type, is_physical_document) VALUES ('Olur Evrakı',false);
INSERT INTO public.documenttypes(document_type, is_physical_document) VALUES ('Diploma',false);