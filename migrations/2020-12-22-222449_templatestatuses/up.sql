CREATE TABLE "templatestatuses" (
  id SERIAL PRIMARY KEY,
  status_name VARCHAR,
  created TIMESTAMP WITH TIME ZONE,
  updated TIMESTAMP WITH TIME ZONE
);

CREATE TRIGGER update_variables_modtime BEFORE INSERT OR UPDATE ON templatestatuses FOR EACH ROW EXECUTE PROCEDURE  update_modified_column();

INSERT INTO public.templatestatuses(status_name) VALUES ('Yayında');
INSERT INTO public.templatestatuses(status_name) VALUES ('Taslak');
INSERT INTO public.templatestatuses(status_name) VALUES ('Yayından Kaldırıldı');
INSERT INTO public.templatestatuses(status_name) VALUES ('Silindi');
