-- Your SQL goes here
CREATE TABLE "templategroups" (
  id SERIAL PRIMARY KEY,
  group_name VARCHAR,
  created TIMESTAMP WITH TIME ZONE,
  updated TIMESTAMP WITH TIME ZONE
);

CREATE TRIGGER update_variables_modtime BEFORE INSERT OR UPDATE ON templategroups FOR EACH ROW EXECUTE PROCEDURE  update_modified_column();

INSERT INTO public.templategroups(group_name) VALUES ('Standart Evrak');
INSERT INTO public.templategroups(group_name) VALUES ('Fiziksel Evrak');
INSERT INTO public.templategroups(group_name) VALUES ('Olur Evrakı');
INSERT INTO public.templategroups(group_name) VALUES ('İzin Şablonu');
INSERT INTO public.templategroups(group_name) VALUES ('Dilekçe');
INSERT INTO public.templategroups(group_name) VALUES ('Yazışma Şablonları');
INSERT INTO public.templategroups(group_name) VALUES ('Diploma');
INSERT INTO public.templategroups(group_name) VALUES ('Proje Takip');
INSERT INTO public.templategroups(group_name) VALUES ('Kararname');