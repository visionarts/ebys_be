-- Your SQL goes here
CREATE TABLE "documents"
(
id SERIAL PRIMARY KEY,
document_name VARCHAR NOT NULL,
document_body VARCHAR NOT NULL,
document_subject VARCHAR NOT NULL,
archieve_code INTEGER  DEFAULT 0 NOT NULL ,
archieve_code_special INTEGER  DEFAULT 0  NOT NULL,
confidentiality INTEGER  DEFAULT 0 NOT NULL,
urgency INTEGER DEFAULT 0 NOT NULL,
created TIMESTAMP WITH TIME ZONE,
updated TIMESTAMP WITH TIME ZONE
);

-- functions
CREATE FUNCTION update_modified_column()
  RETURNS TRIGGER AS $$
  BEGIN
     if  (TG_OP = 'UPDATE') then
        NEW.updated = now();
     else 
        NEW.created = now();
     end if;
    RETURN NEW;
  END;
  $$ language 'plpgsql';
--triggers

CREATE TRIGGER update_templates_modtime BEFORE INSERT OR UPDATE ON documents FOR EACH ROW EXECUTE PROCEDURE  update_modified_column();
