-- Your SQL goes here

CREATE TABLE "templateunits" (
  id SERIAL PRIMARY KEY,
  unit_id INTEGER REFERENCES units NOT NULL,
  template_id INTEGER REFERENCES templates NOT NULL,
  created TIMESTAMP WITH TIME ZONE,
  updated TIMESTAMP WITH TIME ZONE
);

CREATE TRIGGER update_variables_modtime BEFORE INSERT OR UPDATE ON templateunits FOR EACH ROW EXECUTE PROCEDURE  update_modified_column();


-- INSERT INTO public.templateunits(unit_id, template_id) VALUES (1,1);