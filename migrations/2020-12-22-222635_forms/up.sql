-- Your SQL goes here
CREATE TABLE forms (
  id SERIAL PRIMARY KEY,
  form_name VARCHAR,
  created TIMESTAMP WITH TIME ZONE,
  updated TIMESTAMP WITH TIME ZONE
);

CREATE TRIGGER update_variables_modtime BEFORE INSERT OR UPDATE ON forms FOR EACH ROW EXECUTE PROCEDURE  update_modified_column();

INSERT INTO public.forms(id,form_name) VALUES (-1,'default');
