-- Your SQL goes here
CREATE TABLE "variablecontenttypes" (
  id SERIAL PRIMARY KEY,
  content_type_name VARCHAR,
  created TIMESTAMP WITH TIME ZONE,
  updated TIMESTAMP WITH TIME ZONE
);

CREATE TRIGGER update_variables_modtime BEFORE INSERT OR UPDATE ON variablecontenttypes FOR EACH ROW EXECUTE PROCEDURE  update_modified_column();

INSERT INTO public.variablecontenttypes(content_type_name) VALUES ('Tekil İçerik');
INSERT INTO public.variablecontenttypes(content_type_name) VALUES ('Çoğul İçerik');