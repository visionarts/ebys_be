-- Your SQL goes here
CREATE TABLE "variabledatatypes" (
  id SERIAL PRIMARY KEY,
  data_type_name VARCHAR,
  created TIMESTAMP WITH TIME ZONE,
  updated TIMESTAMP WITH TIME ZONE
);

CREATE TRIGGER update_variables_modtime BEFORE INSERT OR UPDATE ON variabledatatypes FOR EACH ROW EXECUTE PROCEDURE  update_modified_column();

INSERT INTO public.variabledatatypes(data_type_name) VALUES ('Metin');
INSERT INTO public.variabledatatypes(data_type_name) VALUES ('Sayı');
INSERT INTO public.variabledatatypes(data_type_name) VALUES ('Tarih');
INSERT INTO public.variabledatatypes(data_type_name) VALUES ('Yazı Editörü');
INSERT INTO public.variabledatatypes(data_type_name) VALUES ('Çoklu Satır');