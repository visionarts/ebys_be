-- Your SQL goes here
CREATE TABLE "variableselectiontypes" (
  id SERIAL PRIMARY KEY,
  selection_type_name VARCHAR,
  created TIMESTAMP WITH TIME ZONE,
  updated TIMESTAMP WITH TIME ZONE
);

CREATE TRIGGER update_variables_modtime BEFORE INSERT OR UPDATE ON variableselectiontypes FOR EACH ROW EXECUTE PROCEDURE  update_modified_column();

INSERT INTO public.variableselectiontypes(selection_type_name) VALUES ('Tekil Seçim');
INSERT INTO public.variableselectiontypes(selection_type_name) VALUES ('Çoğul Seçim');