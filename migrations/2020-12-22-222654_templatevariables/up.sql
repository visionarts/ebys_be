-- Your SQL goes here
CREATE TABLE templatevariables (
  id SERIAL PRIMARY KEY,
  template_id INTEGER REFERENCES templates NOT NULL,
  variable_id INTEGER REFERENCES variables NOT NULL,
  form_id INTEGER REFERENCES forms NOT NULL,
  created TIMESTAMP WITH TIME ZONE,
  updated TIMESTAMP WITH TIME ZONE
);

--triggers
CREATE TRIGGER update_templatevariables_modtime BEFORE INSERT OR UPDATE ON templatevariables FOR EACH ROW EXECUTE PROCEDURE  update_modified_column();