-- Your SQL goes here
CREATE TABLE "variabledisplaytypes" (
  id SERIAL PRIMARY KEY,
  display_type_name VARCHAR,
  created TIMESTAMP WITH TIME ZONE,
  updated TIMESTAMP WITH TIME ZONE
);

CREATE TRIGGER update_variables_modtime BEFORE INSERT OR UPDATE ON variabledisplaytypes FOR EACH ROW EXECUTE PROCEDURE  update_modified_column();

INSERT INTO public.variabledisplaytypes(display_type_name) VALUES ('Açılır Kutu');
INSERT INTO public.variabledisplaytypes(display_type_name) VALUES ('Normal Liste');
